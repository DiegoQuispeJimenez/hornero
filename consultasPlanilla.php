<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/favicon2.png">
    <title>Dashboard RR.HH</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/dashboard.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
  </head>

  <body>
    <header>
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">El Hornero</a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Configuraciones</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Perfil</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Ayuda</a>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
          <ul class="nav nav-pills flex-column">
            <li class="nav-item">
              <a class="nav-link" href="dashboard.html">Vista</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" href="planilla.html">Planilla<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="asistencia.html">Asistencia</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="capacitaciones.php">Capacitaciones</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="reportes.html">Reportes</a>
            </li>
          </ul>
        </nav>
        <main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
          <h1>Consulta de Empleado</h1>
          <form>
            <div class="form-group">
              <label>Codigo de Empleado:</label>
              <input type="" class="form-control col-4" name="codigo_empleado" id=""  placeholder="Codigo de empleado">
            </div>
            <button type="submit" class="btn btn-primary col-1"> Todos </button>
            <button type="reset" class="btn btn-warning col-1" > Limpiar </button>
            <button type="button mostrar-todos" class="btn btn-success col-1"> Buscar </button>
          </form>
          <br>
           <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>DNI</th>
                    <th>Telefono</th>
                    <th>Email</th>
                    <th>Sexo</th>
                    <th>Area</th>
                    <th>Sueldo</th>
                    <th>Turno</th>
                  </tr>
                </thead>
                <tbody>
                <?php 
                  $consulta = "SELECT * FROM empleado_GRUPO4";
                  $ejecutar = sqlsrv_query($con, $consulta);
                  $i = 0;

                  while($fila = sqlsrv_fetch_array($ejecutar)){
                  $id = $fila['emp_id'];
                  $nombres = $fila['emp_nombres'];
                  $apellidos = $fila['emp_apellidos'];
                  $dni = $fila['emp_dni'];
                  $telefono = $fila['emp_telefono_cel'];
                  $email = $fila['emp_email'];
                  $sexo = $fila['emp_sexo'];
                  $area = $fila['emp_area'];
                  $sueldo = $fila['emp_sueldo'];
                  $turno = $fila['emp_turno'];
                  $i++;
                
              ?>
                  <tr>
                    <td><?php echo $id ?></td>
                    <td><?php echo $nombres ?></td>
                    <td><?php echo $apellidos ?></td>
                    <td><?php echo $dni ?></td>
                    <td><?php echo $telefono ?></td>
                    <td><?php echo $email?></td>
                    <td><?php echo $sexo ?></td>
                    <td><?php echo $area ?></td>
                    <td><?php echo $sueldo ?></td>
                    <td><?php echo $turno ?></td>
                  </tr>
                  <?php } ?>

              <?php 
                  $codigo_empleado = $_POST['codigo_empleado'];
                  $consulta = "SELECT * FROM empleado_GRUPO4 where emp_id =$codigo_empleado";
                  $ejecutar = sqlsrv_query($con, $consulta);
                  $i = 0;

                  while($fila = sqlsrv_fetch_array($ejecutar)){
                  $id = $fila['emp_id'];
                  $nombres = $fila['emp_nombres'];
                  $apellidos = $fila['emp_apellidos'];
                  $dni = $fila['emp_dni'];
                  $telefono = $fila['emp_telefono_cel'];
                  $email = $fila['emp_email'];
                  $sexo = $fila['emp_sexo'];
                  $area = $fila['emp_area'];
                  $sueldo = $fila['emp_sueldo'];
                  $turno = $fila['emp_turno'];
                  $i++;
                
              ?>
                  <tr>
                    <td><?php echo $id ?></td>
                    <td><?php echo $nombres ?></td>
                    <td><?php echo $apellidos ?></td>
                    <td><?php echo $dni ?></td>
                    <td><?php echo $telefono ?></td>
                    <td><?php echo $email?></td>
                    <td><?php echo $sexo ?></td>
                    <td><?php echo $area ?></td>
                    <td><?php echo $sueldo ?></td>
                    <td><?php echo $turno ?></td>
                  </tr>
                  <?php } ?>

                </tbody>
              </table>
            </div>
          </main>
        </div>
    </div>
    <script src="js/jquery-3.2.1.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-3.2.1.js"><\/script>')</script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/holder.js"></script>
    <script src="js/react.js"></script>
    <script src="js/vue.js"></script>
    <script src="js/angular.js"></script>
  </body>
</html>