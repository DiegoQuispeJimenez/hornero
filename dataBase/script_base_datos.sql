create table empleado_GRUPO4(
	emp_id char(5) null primary key,
	emp_nombres varchar(50) not null,
	emp_apellidos varchar(50) not null,
	emp_dni int not null,
	emp_telefono_cel int null,
	emp_email varchar(30) not null,
	emp_sexo char(1) not null,
	emp_area varchar(20) not null,
	emp_sueldo money not null,
	emp_turno char(1) not null
	
)
go



--tabla asistencias
create table asistencia_GRUPO4(
	id_asistencia char(5) identity not null primary key,
	emp_id char(5) not null ,-- fk/ empleado
	emp_nombres varchar(50) not null,
	emp_apellidos varchar(50) not null,
	entrada datetime not null,
	salida datetime not null,
	horas int not null,
	turno varchar(15)
)
go

create table capacitaciones_GRUPO4(
	id_capacitaciones varchar() not null  primary key,
	emp_id char(5) not null,
	emp_nombres varchar(50) not null,
	emp_apellidos varchar(50) not null,
	capacitacion_01 char(1) not null,
	capacitacion_02 char(1) not	null,
	capacitacion_03 char(1) not null,
	capacitacion_04 char(1) not null
)
go




create table reportes_GRUPO4(
	id_reportes char(5) not null primary key,
	emp_id char(5) null ,
	emp_nombres varchar(50) not null,
	emp_apellidos varchar(50) not null,
	sueldo money not null,
	observaciones varchar(300)

)
go


--relaciones entre tablas, creando relaciones
Alter table capacitaciones_GRUPO4
ADD CONSTRAINT FK_capacitaciones_GRUPO4_empleado_GRUPO4
FOREIGN KEY (emp_id) REFERENCES empleado_GRUPO4 (emp_id)
go


Alter table asistencia_GRUPO4
ADD CONSTRAINT FK_asistencia_GRUPO4_empleado_GRUPO4
FOREIGN KEY (emp_id) REFERENCES empleado_GRUPO4 (emp_id)
go


Alter table reportes_GRUPO4
ADD CONSTRAINT FK_reportes_GRUPO4_empleado_GRUPO4
FOREIGN KEY (emp_id) REFERENCES empleado_GRUPO4 (emp_id)
go





--botón "registrar empleado" //planilla/registro de empleado
insert into empleado_GRUPO4(emp_nombres,emp_apellidos,emp_dni,emp_telefono_cel,emp_sexo,emp_email,emp_sueldo,emp_area,emp_turno)
values($nombres,$apellidos,$dni,$telefono,$sexo,$email,$sueldo,$area,$turno)
go
--no olvida agregar campos

--botón "buscar", en planilla/consulta Empleado/
select emp_nombres ,emp_apellidos ,emp_dni ,emp_telefono_cel , emp_sexo ,emp_email ,emp_sueldo ,emp_area ,emp_turno from empleado_GRUPO4
where emp_id =$codigo_empleado

--colocar boton que muestre a todos los empleados y limpie registros
--boton mostrar todos
select emp_nombres ,emp_apellidos ,emp_dni ,emp_telefono_cel , emp_sexo ,emp_email ,emp_sueldo ,emp_area ,emp_turno from empleado_GRUPO4




--botón Buscar	Asistencia/Selecciones fecha:
select empleado_GRUPO4.emp_id , (empleado_GRUPO4.emp_nombres + ' ' + empleado_GRUPO4.emp_apellidos) ,
asistencia_GRUPO4.salida,asistencia_GRUPO4.horas  from empleado_GRUPO4 join	asistencia_GRUPO4
on empleado_GRUPO4.emp_id = asistencia_GRUPO4.emp_id
go

--en capacitaciones asumo que es otro select 
select * from capacitaciones_GRUPO4
go


--botón "busca" /Repotes/Empleado.
select  emp_nombres, emp_apellidos, sueldo, observaciones  from capacitaciones_GRUPO4
where emp_apellidos = $apellidos
go
--acuerda de cambiarlo







           








/*notas adicionales       
select emp_nombres as [Nombres] ,emp_apellidos as [Apellidos] ,emp_dni as [DNI] ,emp_telefono_cel as [Teléfono], emp_sexo as [Género] ,emp_email as [E-mail] ,emp_sueldo as [Sueldo S/.] ,emp_area as [Área] ,emp_turno as [Turno] from empleado_GRUPO4
go


--botón Buscar	Asistencia/Selecciones fecha:
select empleado_GRUPO4.emp_id as [Código Emp.], (empleado_GRUPO4.emp_nombres + ' ' + empleado_GRUPO4.emp_apellidos) as [Nombres],
asistencia_GRUPO4.salida as [Salida] from empleado_GRUPO4 join	asistencia_GRUPO4
on empleado_GRUPO4.emp_id = asistencia_GRUPO4.emp_id
go

*/