Create Database Hornero
USE Hornero
GO
/****** Object:  Table [dbo].[ACCESO_GRUPO1]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ACCESO_GRUPO1](
	[id_acceso] [varchar](20) NOT NULL,
	[acc_fechIni] [datetime] NULL,
	[acc_horaIni] [datetime] NULL,
	[acc_fechFin] [datetime] NULL,
	[acchoraFin] [datetime] NULL,
	[id_usuario] [varchar](20) NOT NULL,
 CONSTRAINT [XPKACCESO] PRIMARY KEY CLUSTERED 
(
	[id_acceso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Asistencia_Empleado_GRUPO4]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Asistencia_Empleado_GRUPO4](
	[cod_empleado] [int] NOT NULL,
	[cod_local] [varchar](20) NOT NULL,
	[cod_tipoC] [varchar](20) NOT NULL,
	[cod_turno] [varchar](20) NOT NULL,
	[cod_asistencia] [varchar](20) NOT NULL,
 CONSTRAINT [XPKEmpleado_Asistencia] PRIMARY KEY CLUSTERED 
(
	[cod_empleado] ASC,
	[cod_tipoC] ASC,
	[cod_local] ASC,
	[cod_turno] ASC,
	[cod_asistencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Asistencia_GRUPO4]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Asistencia_GRUPO4](
	[cod_asistencia] [varchar](20) NOT NULL,
	[cod_empleado] [varchar](20) NULL,
	[entrada_marcada] [datetime] NULL,
	[salida_marcada] [datetime] NULL,
	[fecha] [datetime] NULL,
	[cod_horas_extras] [varchar](20) NULL,
	[cod_horario] [varchar](20) NULL,
	[cod_turno] [varchar](20) NOT NULL,
 CONSTRAINT [XPKAsistencia_GRUPO4] PRIMARY KEY CLUSTERED 
(
	[cod_asistencia] ASC,
	[cod_turno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BALANCE_GENERAL_GRUPO9]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BALANCE_GENERAL_GRUPO9](
	[IdLibroInventario] [int] NOT NULL,
	[IdLibroCaja] [int] NOT NULL,
	[IdLibroBanco] [char](18) NOT NULL,
	[IdLibroMayor] [int] NOT NULL,
	[IdCatalogoBalance] [int] NOT NULL,
	[FechInicio] [datetime] NULL,
	[FechFin] [datetime] NULL,
	[Balance] [int] NULL,
	[IdBalanceGeneral] [int] NOT NULL,
 CONSTRAINT [XPKBALANCE_GENERAL_GRUPO9] PRIMARY KEY CLUSTERED 
(
	[IdBalanceGeneral] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BEBIDA_INSUMO_GRUPO5]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BEBIDA_INSUMO_GRUPO5](
	[ID_BEBIDA_INSUMO] [int] NOT NULL,
	[ID_INSUMO] [int] NOT NULL,
	[ID_BEBIDAS] [int] NOT NULL,
 CONSTRAINT [XPKBEBIDA_INSUMO_G5] PRIMARY KEY CLUSTERED 
(
	[ID_BEBIDA_INSUMO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BEBIDAS_GRUPO5]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BEBIDAS_GRUPO5](
	[ID_BEBIDAS] [int] IDENTITY(1,1) NOT NULL,
	[BEB_NOMBRE] [varchar](50) NOT NULL,
	[BEB_PRECIO] [money] NULL,
	[ID_TIPO_BEBIDA] [int] NOT NULL,
 CONSTRAINT [XPKBEBIDAS_G5] PRIMARY KEY CLUSTERED 
(
	[ID_BEBIDAS] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BSC_GRUPO7]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BSC_GRUPO7](
	[codBsc] [int] IDENTITY(1,1) NOT NULL,
	[nombreBsc] [varchar](100) NULL,
	[estadoBsc] [bit] NULL,
	[fechaBsc] [date] NULL,
 CONSTRAINT [XPKBSC] PRIMARY KEY CLUSTERED 
(
	[codBsc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BSC_OBJETIVO_GRUPO7]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BSC_OBJETIVO_GRUPO7](
	[idBscObjetivo] [int] IDENTITY(1,1) NOT NULL,
	[idObjetivo] [int] NULL,
	[codBsc] [int] NULL,
 CONSTRAINT [XPKBSC_OBJETIVO] PRIMARY KEY CLUSTERED 
(
	[idBscObjetivo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Capaciaciones_Empleado_GRUPO4]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Capaciaciones_Empleado_GRUPO4](
	[cod_empleado] [int] NOT NULL,
	[cod_local] [varchar](20) NOT NULL,
	[cod_tipoC] [varchar](20) NOT NULL,
	[cod_turno] [varchar](20) NOT NULL,
	[cod_Capacitacion] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Empleado_Capaciaciones] PRIMARY KEY CLUSTERED 
(
	[cod_empleado] ASC,
	[cod_local] ASC,
	[cod_tipoC] ASC,
	[cod_turno] ASC,
	[cod_Capacitacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Capaciaciones_GRUPO4]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Capaciaciones_GRUPO4](
	[cod_Capacitacion] [varchar](20) NOT NULL,
	[detalle] [varchar](20) NULL,
	[fecha] [datetime] NULL,
	[cod_empleado] [varchar](20) NULL,
	[resultado] [varchar](20) NULL,
 CONSTRAINT [XPKCapaciaciones_GRUPO4] PRIMARY KEY CLUSTERED 
(
	[cod_Capacitacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cargo_GRUPO4]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cargo_GRUPO4](
	[cod_cargo] [varchar](20) NOT NULL,
	[detalle] [varchar](20) NULL,
	[cod_empleado] [int] NOT NULL,
	[cod_tipoC] [varchar](20) NOT NULL,
	[cod_local] [varchar](20) NOT NULL,
	[cod_turno] [varchar](20) NOT NULL,
 CONSTRAINT [XPKCargo_GRUPO4] PRIMARY KEY CLUSTERED 
(
	[cod_cargo] ASC,
	[cod_empleado] ASC,
	[cod_tipoC] ASC,
	[cod_local] ASC,
	[cod_turno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CATALOGO_BALANCE_GENERAL_GRUPO9]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CATALOGO_BALANCE_GENERAL_GRUPO9](
	[Id_SubCuenta] [char](18) NOT NULL,
	[IdCatalogoBalance] [int] NOT NULL,
	[IdLibroMayor] [int] NULL,
	[IdLibCaja] [int] NULL,
	[IdLibrosDiarios] [int] NULL,
	[estado] [varchar](20) NULL,
	[IdLibroBanco] [int] NULL,
 CONSTRAINT [XPKCATALOGO_BALANCE_GENERAL_GRUPO9] PRIMARY KEY CLUSTERED 
(
	[IdCatalogoBalance] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CATALOGO_DE_LIBROS_DIARIOS_GRUPO9]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CATALOGO_DE_LIBROS_DIARIOS_GRUPO9](
	[IdSubCuenta] [char](18) NOT NULL,
	[IDCatLibDiario] [int] NOT NULL,
	[Estado] [int] NULL,
 CONSTRAINT [XPKCATALOGO_DE_LIBROS_DIARIOS_GRUPO9] PRIMARY KEY CLUSTERED 
(
	[IDCatLibDiario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CATALOGO_ESTADO_PERDIDAS_Y_GANANCIAS_GRUPO9]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CATALOGO_ESTADO_PERDIDAS_Y_GANANCIAS_GRUPO9](
	[Id_SubCuenta] [char](18) NOT NULL,
	[IdCatPerdGanancias] [int] NOT NULL,
	[Estado] [int] NULL,
	[IdLibroMayor] [int] NULL,
	[IdLibroInventario] [int] NULL,
	[IdLibroCaja] [int] NULL,
	[IdLibroBanco] [int] NULL,
 CONSTRAINT [XPKCATALOGO_ESTADO_PERDIDAS_Y_GANANCIAS_GRUPO9] PRIMARY KEY CLUSTERED 
(
	[IdCatPerdGanancias] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CATALOGO_LIBRO_BANCO_GRUPO9]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CATALOGO_LIBRO_BANCO_GRUPO9](
	[IdSubCuenta] [char](18) NOT NULL,
	[IdCatLibBanco] [int] NOT NULL,
	[estado] [int] NULL,
 CONSTRAINT [XPKCATALOGO_LIBRO_BANCO_GRUPO9] PRIMARY KEY CLUSTERED 
(
	[IdCatLibBanco] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CATALOGO_LIBRO_CAJA_GRUPO9]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CATALOGO_LIBRO_CAJA_GRUPO9](
	[IdSubCuenta] [char](18) NOT NULL,
	[IdCatLibCaja] [int] NOT NULL,
	[Estado] [int] NULL,
 CONSTRAINT [XPKCATALOGO_LIBRO_CAJA_GRUPO9] PRIMARY KEY CLUSTERED 
(
	[IdCatLibCaja] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CATALOGO_LIBRO_INVENTARIO_GRUPO9]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CATALOGO_LIBRO_INVENTARIO_GRUPO9](
	[IdSubCuenta] [char](18) NOT NULL,
	[IdCatLibInventario] [int] NOT NULL,
	[Estado] [int] NULL,
 CONSTRAINT [XPKCATALOGO_LIBRO_INVENTARIO_GRUPO9] PRIMARY KEY CLUSTERED 
(
	[IdCatLibInventario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CATEGORIA_DEL_CLIENTE_GRUPO3]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CATEGORIA_DEL_CLIENTE_GRUPO3](
	[CategoriaID] [int] NOT NULL,
	[NomCategoria] [varchar](40) NOT NULL,
	[Descipcion] [varchar](200) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CLIENTE_GRUPO3]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CLIENTE_GRUPO3](
	[Id_CLIENTES] [int] NOT NULL,
	[Nombres] [varchar](40) NOT NULL,
	[Apellidos] [varchar](40) NOT NULL,
	[dni_cli] [int] NULL,
	[Direccion] [varchar](40) NULL,
	[telefono] [int] NOT NULL,
	[celular_cli] [int] NULL,
	[correo_cli] [varchar](40) NULL,
	[ReservaID] [int] NULL,
	[PlatoID] [int] NULL,
	[CategoriaID] [int] NOT NULL,
	[ID_MESERO] [int] NOT NULL,
	[Sexo] [varchar](10) NULL,
 CONSTRAINT [XPKCLIENTES_G8] PRIMARY KEY CLUSTERED 
(
	[Id_CLIENTES] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[COMPRAS_GRUPO8]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[COMPRAS_GRUPO8](
	[Id_COMPRAS] [int] NOT NULL,
	[Id_PROVEEDORES] [int] NOT NULL,
	[total_com] [decimal](10, 2) NULL,
	[fecharegistro_com] [datetime] NULL,
	[fechaentrega_com] [datetime] NULL,
	[idcuentaporpagar] [int] NOT NULL,
	[id_compras_libro] [int] NULL,
 CONSTRAINT [XPKCOMPRAS_G8] PRIMARY KEY CLUSTERED 
(
	[Id_COMPRAS] ASC,
	[Id_PROVEEDORES] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[controlchequepropio_GRUPO7F]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[controlchequepropio_GRUPO7F](
	[idchequepropio] [int] NOT NULL,
	[fecha_ingreso] [datetime] NOT NULL,
	[fecha_pago] [datetime] NOT NULL,
	[nombre_banco] [varchar](45) NOT NULL,
	[importe] [decimal](7, 2) NULL,
	[estado] [varchar](20) NOT NULL,
	[idproveedor] [int] NOT NULL,
	[idmediopago] [int] NULL,
 CONSTRAINT [XPKcontrolchequepropio] PRIMARY KEY CLUSTERED 
(
	[idchequepropio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[controlchequetercero_GRUPO7F]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[controlchequetercero_GRUPO7F](
	[idchequetercero] [int] NOT NULL,
	[fecha_ingreso] [datetime] NOT NULL,
	[fecha_pago] [datetime] NOT NULL,
	[nombre_banco] [varchar](45) NOT NULL,
	[importe] [decimal](7, 2) NOT NULL,
	[idcliente] [int] NULL,
	[idmediopago] [int] NULL,
 CONSTRAINT [XPKcontrolchequetercero] PRIMARY KEY CLUSTERED 
(
	[idchequetercero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CUENTASBANCARIAS_GRUPO7F]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CUENTASBANCARIAS_GRUPO7F](
	[idcuentasbancarias] [int] NOT NULL,
	[nombre_banco] [varchar](45) NOT NULL,
	[nro_cuenta] [int] NOT NULL,
	[titular_cuenta] [varchar](45) NOT NULL,
	[fecha] [datetime] NOT NULL,
	[total_deposito] [decimal](7, 2) NOT NULL,
	[total_retiro] [decimal](7, 2) NULL,
	[idcuentaporpagar] [int] NULL,
	[idcuentasporcobrar] [int] NULL,
	[Id_CLIENTES] [int] NOT NULL,
	[Id_COMPRAS] [int] NOT NULL,
	[Id_PROVEEDORES] [int] NOT NULL,
	[id_VENTAS] [int] NOT NULL,
	[Estado] [int] NULL,
 CONSTRAINT [XPKcuentasbancarias] PRIMARY KEY CLUSTERED 
(
	[idcuentasbancarias] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cuentasporcobrar_GRUPO7F]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cuentasporcobrar_GRUPO7F](
	[idcuentasporcobrar] [int] NOT NULL,
	[idcliente] [int] NULL,
	[saldo] [decimal](7, 2) NOT NULL,
	[fecha_apertura] [datetime] NOT NULL,
	[idmediopago] [int] NULL,
 CONSTRAINT [XPKcuentasporcobrar] PRIMARY KEY CLUSTERED 
(
	[idcuentasporcobrar] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[cuentasporpagar_GRUPO7F]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cuentasporpagar_GRUPO7F](
	[idcuentaporpagar] [int] NOT NULL,
	[idproveedor] [int] NOT NULL,
	[saldo] [decimal](7, 2) NOT NULL,
	[fecha_apertura] [datetime] NOT NULL,
 CONSTRAINT [XPKcuentasporpagar] PRIMARY KEY CLUSTERED 
(
	[idcuentaporpagar] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Departamento_GRUPO4]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Departamento_GRUPO4](
	[cod_departamento] [varchar](20) NOT NULL,
	[detalle] [varchar](20) NULL,
	[cod_empleado] [int] NOT NULL,
	[cod_tipoC] [varchar](20) NOT NULL,
	[cod_local] [varchar](20) NOT NULL,
	[cod_turno] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Departamento_GRUPO4] PRIMARY KEY CLUSTERED 
(
	[cod_departamento] ASC,
	[cod_empleado] ASC,
	[cod_tipoC] ASC,
	[cod_local] ASC,
	[cod_turno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Descanso_Medico_GRUPO4]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Descanso_Medico_GRUPO4](
	[cod_descanMed] [varchar](20) NOT NULL,
	[cod_empleado] [int] NOT NULL,
	[tiempo_descanso] [int] NULL,
	[seguro] [varchar](20) NULL,
	[cod_planilla] [varchar](20) NOT NULL,
	[cod_tipoC] [varchar](20) NOT NULL,
	[cod_local] [varchar](20) NOT NULL,
	[cod_turno] [varchar](20) NOT NULL,
 CONSTRAINT [XPKDescanso_Medico_GRUPO4] PRIMARY KEY CLUSTERED 
(
	[cod_descanMed] ASC,
	[cod_planilla] ASC,
	[cod_empleado] ASC,
	[cod_tipoC] ASC,
	[cod_local] ASC,
	[cod_turno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Descuento_GRUPO4]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Descuento_GRUPO4](
	[cod_desc] [varchar](20) NOT NULL,
	[cod_empleado] [varchar](20) NULL,
	[detalle] [varchar](20) NULL,
	[descuentos] [int] NULL,
	[dias_faltados] [int] NULL,
	[sanciones] [varchar](20) NULL,
 CONSTRAINT [XPKDescuento_GRUPO4] PRIMARY KEY CLUSTERED 
(
	[cod_desc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Empleado_GRUPO4]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Empleado_GRUPO4](
	[cod_empleado] [int] NOT NULL,
	[nombre] [varchar](20) NULL,
	[apellido] [varchar](20) NULL,
	[dni] [int] NOT NULL,
	[sexo] [varchar](20) NULL,
	[email] [varchar](20) NULL,
	[telefono] [int] NULL,
	[cod_cargo] [varchar](20) NOT NULL,
	[cod_horario] [varchar](20) NULL,
	[cod_local] [varchar](20) NOT NULL,
	[cod_tipoC] [varchar](20) NOT NULL,
	[cod_turno] [varchar](20) NOT NULL,
	[id_perfil] [varchar](20) NOT NULL,
	[id_usuario] [varchar](20) NOT NULL,
	[ID_VENTA] [int] NOT NULL,
	[codResponsable] [int] NULL,
	[ID_LOCAL] [int] NOT NULL,
 CONSTRAINT [PK_Empleado_GRUPO4_1] PRIMARY KEY CLUSTERED 
(
	[cod_empleado] ASC,
	[cod_local] ASC,
	[cod_tipoC] ASC,
	[cod_turno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ESTADO_DE_PERDIDAS_Y_GANANCIAS_GRUPO9]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ESTADO_DE_PERDIDAS_Y_GANANCIAS_GRUPO9](
	[IdLibrosDiarios] [int] NOT NULL,
	[IdLibroInventario] [int] NOT NULL,
	[IdLibroCaja] [int] NOT NULL,
	[IdLibroBanco] [char](18) NOT NULL,
	[IdCatPerdGanancias] [int] NOT NULL,
	[FechInicio] [datetime] NULL,
	[FechFin] [datetime] NULL,
	[BalanceActivo] [int] NULL,
	[BalancePasivo] [char](18) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Evaluación_de_desempeño_GRUPO4]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Evaluación_de_desempeño_GRUPO4](
	[cod_evaluacion] [varchar](20) NOT NULL,
	[detalle] [varchar](20) NULL,
	[fecha] [datetime] NULL,
	[cod_empleado] [int] NOT NULL,
	[resultado] [varchar](20) NULL,
	[cod_tipoC] [varchar](20) NOT NULL,
	[cod_local] [varchar](20) NOT NULL,
	[cod_turno] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Evaluación_de_desempeño_GRUPO4_1] PRIMARY KEY CLUSTERED 
(
	[cod_evaluacion] ASC,
	[cod_empleado] ASC,
	[cod_tipoC] ASC,
	[cod_local] ASC,
	[cod_turno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Gratificación_GRUPO4]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Gratificación_GRUPO4](
	[cod_gratificacion] [varchar](20) NOT NULL,
	[cod_empleado] [int] NOT NULL,
	[tiempo_laboral] [int] NULL,
	[cod_planilla] [varchar](20) NOT NULL,
	[cod_tipoC] [varchar](20) NOT NULL,
	[cod_local] [varchar](20) NOT NULL,
	[cod_turno] [varchar](20) NOT NULL,
 CONSTRAINT [XPKGratificación_GRUPO4] PRIMARY KEY CLUSTERED 
(
	[cod_gratificacion] ASC,
	[cod_planilla] ASC,
	[cod_empleado] ASC,
	[cod_tipoC] ASC,
	[cod_local] ASC,
	[cod_turno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Horario_GRUPO4]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Horario_GRUPO4](
	[cod_turno] [varchar](20) NOT NULL,
	[hora_entrada] [datetime] NULL,
	[hora_salida] [datetime] NULL,
 CONSTRAINT [XPKHorario_GRUPO4] PRIMARY KEY CLUSTERED 
(
	[cod_turno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Horario_Horas_extras_GRUPO4]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Horario_Horas_extras_GRUPO4](
	[cod_turno] [varchar](20) NOT NULL,
	[cod_horas_extras] [varchar](20) NOT NULL,
	[cod_asistencia] [varchar](20) NOT NULL,
 CONSTRAINT [XPKHorario_Horas_extras] PRIMARY KEY CLUSTERED 
(
	[cod_turno] ASC,
	[cod_horas_extras] ASC,
	[cod_asistencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Horas_extras_GRUPO4]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Horas_extras_GRUPO4](
	[cod_horas_extras] [varchar](20) NOT NULL,
	[cod_empleado] [varchar](20) NULL,
	[num_horas_extras] [int] NULL,
	[fecha] [datetime] NULL,
	[cod_asistencia] [varchar](20) NOT NULL,
	[cod_turno] [varchar](20) NOT NULL,
 CONSTRAINT [XPKHoras_extras_GRUPO4] PRIMARY KEY CLUSTERED 
(
	[cod_horas_extras] ASC,
	[cod_asistencia] ASC,
	[cod_turno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[INDICADOR_GRUPO7]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[INDICADOR_GRUPO7](
	[idIndicador] [int] IDENTITY(1,1) NOT NULL,
	[descripcionIndicador] [varchar](100) NULL,
	[valorObjIndicador] [float] NULL,
	[resultadoIndicador] [int] NULL,
	[idObjetivo] [int] NOT NULL,
	[codResponsable] [int] NULL,
	[valorRealIndicador] [float] NULL,
	[fechaIndicador] [date] NULL,
 CONSTRAINT [XPKINDICADOR] PRIMARY KEY CLUSTERED 
(
	[idIndicador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[INSUMO_GRUPO5]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[INSUMO_GRUPO5](
	[ID_INSUMO] [int] IDENTITY(1,1) NOT NULL,
	[INS_NOMBRE] [varchar](200) NOT NULL,
	[INS_CANTIDAD] [int] NOT NULL,
	[Id_PROVEEDORES] [int] NOT NULL,
 CONSTRAINT [XPKINSUMO_G5] PRIMARY KEY CLUSTERED 
(
	[ID_INSUMO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[INSUMO_PLATO_GRUPO5]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[INSUMO_PLATO_GRUPO5](
	[ID_PLATO_INSUMO] [int] NOT NULL,
	[ID_INSUMO] [int] NOT NULL,
	[ID_PLATO] [int] NOT NULL,
 CONSTRAINT [XPKPLATO_INSUMO_G5] PRIMARY KEY CLUSTERED 
(
	[ID_PLATO_INSUMO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LIBRO_BANCO_GRUPO9]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LIBRO_BANCO_GRUPO9](
	[IdCatLibBanco] [int] NOT NULL,
	[IdLibroBanco] [char](18) NOT NULL,
	[Estado] [int] NULL,
	[FechaBanco] [datetime] NULL,
 CONSTRAINT [XPKLIBRO_BANCO_GRUPO9] PRIMARY KEY CLUSTERED 
(
	[IdLibroBanco] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LIBRO_CAJA_GRUPO9]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LIBRO_CAJA_GRUPO9](
	[IdCatLibCaja] [int] NOT NULL,
	[IdLibroCaja] [int] NOT NULL,
	[Estado] [int] NULL,
	[RegistroCaja] [datetime] NULL,
 CONSTRAINT [XPKLIBRO_CAJA_GRUPO9] PRIMARY KEY CLUSTERED 
(
	[IdLibroCaja] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LIBRO_DIARIO_GRUPO9]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LIBRO_DIARIO_GRUPO9](
	[IDCatLibDiario] [int] NOT NULL,
	[Estado] [int] NULL,
	[fechaIngreso] [datetime] NULL,
	[IdLibrosDiarios] [int] NOT NULL,
	[IdVentasComprasLibros] [int] NOT NULL,
 CONSTRAINT [XPKLIBRO_DIARIO_GRUPO9] PRIMARY KEY CLUSTERED 
(
	[IdLibrosDiarios] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LIBRO_INVENTARIO_GRUPO9]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LIBRO_INVENTARIO_GRUPO9](
	[IdCatLibInventario] [int] NOT NULL,
	[IdLibroInventario] [int] NOT NULL,
	[Estado] [int] NULL,
	[RegistroInventario] [int] NULL,
	[Id_PROVEEDORES] [int] NOT NULL,
	[Id_COMPRAS] [int] NOT NULL,
	[IdCuentas] [varchar](20) NOT NULL,
 CONSTRAINT [XPKLIBRO_INVENTARIO_GRUPO9] PRIMARY KEY CLUSTERED 
(
	[IdLibroInventario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LIBRO_MAYOR_GRUPO9]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LIBRO_MAYOR_GRUPO9](
	[IdLibrosDiarios] [int] NOT NULL,
	[IdLibroMayor] [int] NOT NULL,
	[Fecha] [datetime] NULL,
 CONSTRAINT [XPKLIBRO_MAYOR_GRUPO9] PRIMARY KEY CLUSTERED 
(
	[IdLibroMayor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LIBROS_DE_VENTAS_COMPRAS_GRUPO9]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LIBROS_DE_VENTAS_COMPRAS_GRUPO9](
	[id_VENTAS] [int] NOT NULL,
	[IdVentasComprasLibros] [int] NOT NULL,
	[Nombre] [varchar](20) NULL,
	[Estado] [int] NULL,
	[Fecha] [datetime] NULL,
	[Id_CLIENTES] [int] NULL,
 CONSTRAINT [XPKVENTAS_COMPRAS_LIBROS_GRUPO9] PRIMARY KEY CLUSTERED 
(
	[IdVentasComprasLibros] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LINEADETALLESCOMPRA_GRUPO8]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LINEADETALLESCOMPRA_GRUPO8](
	[Id_LINEADETALLESCOMPRA] [int] NOT NULL,
	[Id_COMPRAS] [int] NOT NULL,
	[Id_PRODUCTOS] [int] NOT NULL,
	[precio] [decimal](10, 2) NULL,
	[cantidad] [int] NULL,
	[Id_PROVEEDORES] [int] NOT NULL,
 CONSTRAINT [PK_LINEADETALLESCOMPRA_GRUPO8_1] PRIMARY KEY CLUSTERED 
(
	[Id_LINEADETALLESCOMPRA] ASC,
	[Id_COMPRAS] ASC,
	[Id_PRODUCTOS] ASC,
	[Id_PROVEEDORES] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LINEADETALLEVENTA_GRUPO8]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LINEADETALLEVENTA_GRUPO8](
	[Id_LINEADETALLEVENTA] [int] NOT NULL,
	[Id_PRODUCTOS] [int] NOT NULL,
	[Id_VENTAS] [int] NOT NULL,
	[precio_lin] [decimal](10, 2) NULL,
	[cantidad_lin] [int] NULL,
	[Id_CLIENTES] [int] NOT NULL,
	[id_promo] [int] NOT NULL,
 CONSTRAINT [PK_LINEADETALLEVENTA_GRUPO8_1] PRIMARY KEY CLUSTERED 
(
	[Id_LINEADETALLEVENTA] ASC,
	[Id_PRODUCTOS] ASC,
	[Id_VENTAS] ASC,
	[Id_CLIENTES] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Local_GRUPO8P]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Local_GRUPO8P](
	[ID_LOCAL] [int] NOT NULL,
	[detalle] [varchar](20) NULL,
	[cod_empleado] [int] NULL,
	[ID_PEDIDO] [nchar](10) NULL,
	[LOC_DIRECCION] [nchar](10) NULL,
	[LOC_NOMBRE] [nchar](10) NULL,
	[Id_CLIENTES] [int] NOT NULL,
 CONSTRAINT [XPKLocal_GRUPO4] PRIMARY KEY CLUSTERED 
(
	[ID_LOCAL] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mediopago_GRUPO7F]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mediopago_GRUPO7F](
	[idmediopago] [int] NOT NULL,
	[nombremediopago] [varchar](40) NOT NULL,
 CONSTRAINT [XPKmediopago] PRIMARY KEY CLUSTERED 
(
	[idmediopago] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MESA_GRUPO5]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MESA_GRUPO5](
	[ID_MESA] [int] IDENTITY(1,1) NOT NULL,
	[MES_CANTIDAD] [int] NOT NULL,
	[ID_LOCAL] [int] NOT NULL,
	[ReservaID] [int] NULL,
 CONSTRAINT [XPKMESA_G5] PRIMARY KEY CLUSTERED 
(
	[ID_MESA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MESERO_GRUPO10]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MESERO_GRUPO10](
	[ID_MESERO] [int] NOT NULL,
	[ID_CLIENTES] [int] NOT NULL,
	[AÑADE] [varchar](30) NULL,
	[MODIFCA] [varchar](30) NULL,
	[ELIMINA] [varchar](30) NULL,
	[BUSCA] [varchar](30) NULL,
	[ID_PEDIDO] [int] NULL,
	[cod_empleado] [int] NULL,
 CONSTRAINT [PK_MESERO_GRUPO10] PRIMARY KEY CLUSTERED 
(
	[ID_MESERO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MODULOS_GRUPO1]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MODULOS_GRUPO1](
	[id_modulo] [varchar](6) NOT NULL,
	[mod_nombre] [varchar](25) NULL,
	[mod_descripcion] [varchar](200) NULL,
	[mod_fechActualiz] [datetime] NULL,
	[mod_horaActualiz] [datetime] NULL,
	[mod_encargado] [varchar](50) NULL,
	[mod_version] [real] NULL,
	[mod_estado] [varchar](1) NULL,
 CONSTRAINT [XPKMODULOS] PRIMARY KEY CLUSTERED 
(
	[id_modulo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MOVIMIENTOS_GRUPO8]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MOVIMIENTOS_GRUPO8](
	[Id_MOVIMIENTOS] [int] NOT NULL,
	[Id_PRODUCTOS] [int] NOT NULL,
	[cantidadIngreso] [int] NULL,
	[Id_COMPRAS] [int] NOT NULL,
	[Id_VENTAS] [int] NOT NULL,
	[Id_CLIENTES] [int] NOT NULL,
	[Id_PROVEEDORES] [int] NOT NULL,
	[cantidadSalida] [char](18) NULL,
 CONSTRAINT [XPKMOVIMIENTOS] PRIMARY KEY CLUSTERED 
(
	[Id_MOVIMIENTOS] ASC,
	[Id_PRODUCTOS] ASC,
	[Id_VENTAS] ASC,
	[Id_CLIENTES] ASC,
	[Id_COMPRAS] ASC,
	[Id_PROVEEDORES] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OBJETIVO_GRUPO7]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OBJETIVO_GRUPO7](
	[idObjetivo] [int] IDENTITY(1,1) NOT NULL,
	[descripcionObjetivo] [varchar](255) NULL,
	[idPerspectiva] [int] NOT NULL,
	[fechaObjetivo] [date] NULL,
 CONSTRAINT [XPKOBJETIVO] PRIMARY KEY CLUSTERED 
(
	[idObjetivo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ORDENPEDIDOS_GRUPO8]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ORDENPEDIDOS_GRUPO8](
	[Id_ORDENPEDIDOS] [int] NOT NULL,
	[Id_PRODUCTOS] [int] NOT NULL,
	[cantidad_orpe] [int] NULL,
	[fecha_orpe] [datetime] NULL,
 CONSTRAINT [XPKORDENPEDIDOS] PRIMARY KEY CLUSTERED 
(
	[Id_ORDENPEDIDOS] ASC,
	[Id_PRODUCTOS] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PEDIDO_BEBIDA_GRUPO5]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PEDIDO_BEBIDA_GRUPO5](
	[ID_PEDIDO_BEBIDA] [int] NOT NULL,
	[CANTIDAD] [int] NOT NULL,
	[ID_PEDIDO] [int] NOT NULL,
	[ID_BEBIDAS] [int] NOT NULL,
 CONSTRAINT [XPKPEDIDO_BEBIDA_G5] PRIMARY KEY CLUSTERED 
(
	[ID_PEDIDO_BEBIDA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PEDIDO_GRUPO5]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PEDIDO_GRUPO5](
	[ID_PEDIDO] [int] IDENTITY(1,1) NOT NULL,
	[PED_FECH] [datetime] NOT NULL,
	[ID_MESA] [int] NOT NULL,
	[Id_CLIENTES] [int] NOT NULL,
	[ID_EMPLEADO] [int] NOT NULL,
	[ID_LOCAL] [int] NULL,
 CONSTRAINT [XPKPEDIDO_G5] PRIMARY KEY CLUSTERED 
(
	[ID_PEDIDO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PEDIDO_PLATO_GRUPO5]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PEDIDO_PLATO_GRUPO5](
	[ID_PEDIDO_PLATO] [int] NOT NULL,
	[CANTIDAD] [int] NOT NULL,
	[ID_PEDIDO] [int] NOT NULL,
	[ID_PLATO] [int] NOT NULL,
 CONSTRAINT [XPKPEDIDO_PLATO_G5] PRIMARY KEY CLUSTERED 
(
	[ID_PEDIDO_PLATO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PERMISO_GRUPO1]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PERMISO_GRUPO1](
	[id_permiso] [varchar](4) NOT NULL,
	[per_nombre] [varchar](20) NULL,
	[per_descripcion] [varchar](40) NULL,
	[per_acceso] [varchar](1) NULL,
	[per_leer] [varchar](1) NULL,
	[per_crear] [varchar](1) NULL,
	[per_modificar] [varchar](1) NULL,
	[per_eliminar] [varchar](1) NULL,
	[per_estado] [varchar](1) NULL,
 CONSTRAINT [XPKPERMISO] PRIMARY KEY CLUSTERED 
(
	[id_permiso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PERSPECTIVA_GRUPO7]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PERSPECTIVA_GRUPO7](
	[idPerspectiva] [int] IDENTITY(1,1) NOT NULL,
	[nombrePerspectiva] [varchar](25) NULL,
 CONSTRAINT [XPKPERSPECTIVA] PRIMARY KEY CLUSTERED 
(
	[idPerspectiva] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Planilla_Asistencia_GRUPO4]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Planilla_Asistencia_GRUPO4](
	[cod_planilla] [varchar](20) NOT NULL,
	[cod_empleado] [int] NOT NULL,
	[cod_tipoC] [varchar](20) NOT NULL,
	[cod_local] [varchar](20) NOT NULL,
	[cod_turno] [varchar](20) NOT NULL,
	[cod_asistencia] [varchar](20) NOT NULL,
 CONSTRAINT [XPKPlanilla_Asistencia] PRIMARY KEY CLUSTERED 
(
	[cod_planilla] ASC,
	[cod_empleado] ASC,
	[cod_tipoC] ASC,
	[cod_local] ASC,
	[cod_turno] ASC,
	[cod_asistencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Planilla_Descuento_GRUPO4]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Planilla_Descuento_GRUPO4](
	[cod_planilla] [varchar](20) NOT NULL,
	[cod_empleado] [int] NOT NULL,
	[cod_tipoC] [varchar](20) NOT NULL,
	[cod_local] [varchar](20) NOT NULL,
	[cod_turno] [varchar](20) NOT NULL,
	[cod_desc] [varchar](20) NOT NULL,
 CONSTRAINT [XPKPlanilla_Descuento] PRIMARY KEY CLUSTERED 
(
	[cod_planilla] ASC,
	[cod_empleado] ASC,
	[cod_tipoC] ASC,
	[cod_local] ASC,
	[cod_turno] ASC,
	[cod_desc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Planilla_GRUPO4]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Planilla_GRUPO4](
	[cod_planilla] [varchar](20) NOT NULL,
	[cod_empleado] [int] NOT NULL,
	[sueldo] [int] NULL,
	[cod_tipoC] [varchar](20) NOT NULL,
	[cod_local] [varchar](20) NOT NULL,
	[cod_turno] [varchar](20) NOT NULL,
 CONSTRAINT [XPKPlanilla_GRUPO4] PRIMARY KEY CLUSTERED 
(
	[cod_planilla] ASC,
	[cod_empleado] ASC,
	[cod_tipoC] ASC,
	[cod_local] ASC,
	[cod_turno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PLATO_GRUPO5]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PLATO_GRUPO5](
	[ID_PLATO] [int] IDENTITY(1,1) NOT NULL,
	[PLA_NOMBRE] [varchar](50) NOT NULL,
	[PLA_PRECIO] [money] NOT NULL,
	[ID_TIPO_PLATO] [int] NOT NULL,
 CONSTRAINT [XPKPLATO_G5] PRIMARY KEY CLUSTERED 
(
	[ID_PLATO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PRODUCTOS_GRUPO8]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PRODUCTOS_GRUPO8](
	[Id_PRODUCTOS] [int] NOT NULL,
	[precio_produc] [decimal](10, 2) NULL,
	[nombre_produc] [varchar](200) NULL,
	[descripcion_produc] [varchar](200) NULL,
	[stock_produc] [int] NULL,
	[stockminimo_produc] [int] NULL,
	[peso_produc] [decimal](10, 2) NULL,
	[tipoproducto_produc] [varchar](45) NULL,
 CONSTRAINT [XPKPRODUCTOS_G8] PRIMARY KEY CLUSTERED 
(
	[Id_PRODUCTOS] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PROMOCION_VENTAS5P]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PROMOCION_VENTAS5P](
	[cantidad] [int] NOT NULL,
	[id_promo] [int] NOT NULL,
	[nom_prom] [varchar](50) NOT NULL,
	[Id_PRODUCTOS] [int] NULL,
	[codBsc] [int] NOT NULL,
 CONSTRAINT [PK_PROMOCION_VENTAS5P_1] PRIMARY KEY CLUSTERED 
(
	[id_promo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PROVEEDORES_GRUPO8]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PROVEEDORES_GRUPO8](
	[Id_PROVEEDORES] [int] NOT NULL,
	[razonsocial_prov] [varchar](200) NULL,
	[ruc_prov] [bigint] NULL,
	[telefono_prov] [int] NULL,
	[direccion_prov] [varchar](200) NULL,
	[correo_prov] [varchar](200) NULL,
	[nombrecontacto_prov] [varchar](200) NULL,
	[dnicontacto_prov] [int] NULL,
	[telefonocontacto_prov] [int] NULL,
	[ID_INSUMO] [int] NOT NULL,
 CONSTRAINT [XPKPROVEEDORES_G8] PRIMARY KEY CLUSTERED 
(
	[Id_PROVEEDORES] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PROVEEDORESXPRODUCTO_GRUPO8]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PROVEEDORESXPRODUCTO_GRUPO8](
	[Id_PROVEEDORESXPRODUCTO] [int] NOT NULL,
	[Id_PRODUCTOS] [int] NOT NULL,
	[Id_PROVEEDORES] [int] NOT NULL,
	[precio_propro] [decimal](10, 2) NULL,
	[fechainicio_propro] [datetime] NULL,
 CONSTRAINT [XPKPROVEEDORESXPRODUCTO] PRIMARY KEY CLUSTERED 
(
	[Id_PROVEEDORESXPRODUCTO] ASC,
	[Id_PRODUCTOS] ASC,
	[Id_PROVEEDORES] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RECETAS_GRUPO8]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RECETAS_GRUPO8](
	[Id_RECETAS] [int] NOT NULL,
 CONSTRAINT [XPKRECETAS] PRIMARY KEY CLUSTERED 
(
	[Id_RECETAS] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RECETAXPRODUCTOS_GRUPO8]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RECETAXPRODUCTOS_GRUPO8](
	[Id_RECETAXPRODUCTOS] [int] NOT NULL,
	[Id_RECETAS] [int] NOT NULL,
	[Id_PRODUCTOS] [int] NOT NULL,
	[cantidad_recpro] [float] NULL,
 CONSTRAINT [XPKRECETAXPRODUCTOS] PRIMARY KEY CLUSTERED 
(
	[Id_RECETAXPRODUCTOS] ASC,
	[Id_PRODUCTOS] ASC,
	[Id_RECETAS] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RESPONSABLE_GRUPO7]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RESPONSABLE_GRUPO7](
	[codResponsable] [int] NOT NULL,
	[nombreResponsable] [varchar](60) NULL,
	[apellidosResponsable] [varchar](60) NULL,
	[cargoResponsable] [varchar](30) NULL,
 CONSTRAINT [XPKRESPONSABLE] PRIMARY KEY CLUSTERED 
(
	[codResponsable] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Revervas_GRUPO3]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Revervas_GRUPO3](
	[ReservaID] [int] NOT NULL,
	[Id_CLIENTES] [int] NOT NULL,
	[FechaPedido] [datetime] NOT NULL,
	[FechaReserva] [datetime] NOT NULL,
 CONSTRAINT [PK_REVERVAS] PRIMARY KEY CLUSTERED 
(
	[ReservaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SOLICITUDCOMPRAS_GRUPO8]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SOLICITUDCOMPRAS_GRUPO8](
	[Id_SOLICITUDCOMPRAS] [int] NOT NULL,
	[Id_PRODUCTOS] [int] NOT NULL,
	[cantidad_solcom] [int] NULL,
	[fecharegistro_solcom] [datetime] NULL,
	[fechaatencion_solcom] [datetime] NULL,
	[Id_COMPRAS] [int] NOT NULL,
	[Id_PROVEEDORES] [int] NOT NULL,
 CONSTRAINT [XPKSOLICITUDCOMPRAS] PRIMARY KEY CLUSTERED 
(
	[Id_SOLICITUDCOMPRAS] ASC,
	[Id_PRODUCTOS] ASC,
	[Id_COMPRAS] ASC,
	[Id_PROVEEDORES] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SUBCUENTAS_GRUPO9]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SUBCUENTAS_GRUPO9](
	[IdCuentas] [varchar](20) NOT NULL,
	[IdSubCuenta] [char](18) NOT NULL,
	[Nombre] [varchar](20) NULL,
	[Estado] [int] NULL,
	[idcuentasbancarias] [int] NOT NULL,
 CONSTRAINT [XPKSUBCUENTAS_GRUPO9] PRIMARY KEY CLUSTERED 
(
	[IdSubCuenta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TIPO_BEBIDA_GRUPO5]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TIPO_BEBIDA_GRUPO5](
	[ID_TIPO_BEBIDA] [int] IDENTITY(1,1) NOT NULL,
	[TIPOB_NOMBRE] [varchar](50) NOT NULL,
	[DESCRIPCION] [varchar](200) NULL,
 CONSTRAINT [XPKTIPO_BEBIDA_G5] PRIMARY KEY CLUSTERED 
(
	[ID_TIPO_BEBIDA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tipo_de_contrato_GRUPO4]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tipo_de_contrato_GRUPO4](
	[cod_tipoC] [varchar](20) NOT NULL,
	[tiempo_contratacion] [int] NOT NULL,
 CONSTRAINT [XPKTipo_de_contrato_GRUPO4] PRIMARY KEY CLUSTERED 
(
	[cod_tipoC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TIPO_PLATO_GRUPO5]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TIPO_PLATO_GRUPO5](
	[ID_TIPO_PLATO] [int] NOT NULL,
	[TIPOP_NOMBRE] [varchar](50) NOT NULL,
	[DESCRIPCION] [varchar](200) NULL,
 CONSTRAINT [XPKTIPO_PLATO_G5] PRIMARY KEY CLUSTERED 
(
	[ID_TIPO_PLATO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[USUA_MODUL_GRUPO1]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[USUA_MODUL_GRUPO1](
	[id_modusu] [varchar](10) NOT NULL,
	[id_usuario] [varchar](6) NULL,
	[id_modulo] [varchar](6) NULL,
 CONSTRAINT [XPKUSUA_MODUL] PRIMARY KEY CLUSTERED 
(
	[id_modusu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[USUARIO_CONTRA_GRUPO4]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[USUARIO_CONTRA_GRUPO4](
	[id_usuario] [varchar](20) NOT NULL,
	[id_estado] [varchar](20) NOT NULL,
	[id_password] [nchar](10) NOT NULL,
	[id_acceso] [varchar](20) NOT NULL,
	[id_permiso] [varchar](4) NOT NULL,
	[id_modusu] [varchar](10) NOT NULL,
	[cod_empleado] [int] NOT NULL,
	[cod_turno] [varchar](20) NOT NULL,
	[cod_local] [varchar](20) NOT NULL,
	[cod_tipoC] [varchar](20) NULL,
 CONSTRAINT [PK_USUARIO_CONTRA_GRUPO4] PRIMARY KEY CLUSTERED 
(
	[id_usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VENTA_GRUPO5]    Script Date: 09/11/2017 02:12:08 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VENTA_GRUPO5](
	[id_VENTAS] [int] NOT NULL,
	[ID_PEDIDO] [int] NOT NULL,
	[FECHA_DE_VENTA] [date] NULL,
	[NUM_DOC] [int] NOT NULL,
	[cod_empleado] [int] IDENTITY(1,1) NOT NULL,
	[id_cliente] [varchar](20) NULL,
	[idcuentasporcobrar] [int] NOT NULL,
	[Id_CLIENTES] [int] NOT NULL,
	[total_ven] [decimal](10, 2) NULL,
	[fecharegistro_ven] [datetime] NULL,
	[fechaentrega_ven] [datetime] NULL,
 CONSTRAINT [PK_VENTA_GRUPO5] PRIMARY KEY CLUSTERED 
(
	[id_VENTAS] ASC,
	[Id_CLIENTES] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [PK_CATEGORIA]    Script Date: 09/11/2017 02:12:08 p.m. ******/
ALTER TABLE [dbo].[CATEGORIA_DEL_CLIENTE_GRUPO3] ADD  CONSTRAINT [PK_CATEGORIA] PRIMARY KEY NONCLUSTERED 
(
	[CategoriaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ACCESO_GRUPO1]  WITH CHECK ADD  CONSTRAINT [FK_ACCESO_GRUPO1_USUARIO_CONTRA_GRUPO41] FOREIGN KEY([id_usuario])
REFERENCES [dbo].[USUARIO_CONTRA_GRUPO4] ([id_usuario])
GO
ALTER TABLE [dbo].[ACCESO_GRUPO1] CHECK CONSTRAINT [FK_ACCESO_GRUPO1_USUARIO_CONTRA_GRUPO41]
GO
ALTER TABLE [dbo].[Asistencia_Empleado_GRUPO4]  WITH CHECK ADD  CONSTRAINT [R_26] FOREIGN KEY([cod_asistencia], [cod_turno])
REFERENCES [dbo].[Asistencia_GRUPO4] ([cod_asistencia], [cod_turno])
GO
ALTER TABLE [dbo].[Asistencia_Empleado_GRUPO4] CHECK CONSTRAINT [R_26]
GO
ALTER TABLE [dbo].[Asistencia_GRUPO4]  WITH CHECK ADD  CONSTRAINT [R_16_GRUPO4] FOREIGN KEY([cod_turno])
REFERENCES [dbo].[Horario_GRUPO4] ([cod_turno])
GO
ALTER TABLE [dbo].[Asistencia_GRUPO4] CHECK CONSTRAINT [R_16_GRUPO4]
GO
ALTER TABLE [dbo].[BALANCE_GENERAL_GRUPO9]  WITH CHECK ADD  CONSTRAINT [R_17_GRUPO9] FOREIGN KEY([IdLibroMayor])
REFERENCES [dbo].[LIBRO_MAYOR_GRUPO9] ([IdLibroMayor])
GO
ALTER TABLE [dbo].[BALANCE_GENERAL_GRUPO9] CHECK CONSTRAINT [R_17_GRUPO9]
GO
ALTER TABLE [dbo].[BALANCE_GENERAL_GRUPO9]  WITH CHECK ADD  CONSTRAINT [R_18_GRUPO9] FOREIGN KEY([IdLibroInventario])
REFERENCES [dbo].[LIBRO_INVENTARIO_GRUPO9] ([IdLibroInventario])
GO
ALTER TABLE [dbo].[BALANCE_GENERAL_GRUPO9] CHECK CONSTRAINT [R_18_GRUPO9]
GO
ALTER TABLE [dbo].[BALANCE_GENERAL_GRUPO9]  WITH CHECK ADD  CONSTRAINT [R_19_GRUPO9] FOREIGN KEY([IdLibroCaja])
REFERENCES [dbo].[LIBRO_CAJA_GRUPO9] ([IdLibroCaja])
GO
ALTER TABLE [dbo].[BALANCE_GENERAL_GRUPO9] CHECK CONSTRAINT [R_19_GRUPO9]
GO
ALTER TABLE [dbo].[BALANCE_GENERAL_GRUPO9]  WITH CHECK ADD  CONSTRAINT [R_20_GRUPO9] FOREIGN KEY([IdLibroBanco])
REFERENCES [dbo].[LIBRO_BANCO_GRUPO9] ([IdLibroBanco])
GO
ALTER TABLE [dbo].[BALANCE_GENERAL_GRUPO9] CHECK CONSTRAINT [R_20_GRUPO9]
GO
ALTER TABLE [dbo].[BALANCE_GENERAL_GRUPO9]  WITH CHECK ADD  CONSTRAINT [R_25_GRUPO9] FOREIGN KEY([IdCatalogoBalance])
REFERENCES [dbo].[CATALOGO_BALANCE_GENERAL_GRUPO9] ([IdCatalogoBalance])
GO
ALTER TABLE [dbo].[BALANCE_GENERAL_GRUPO9] CHECK CONSTRAINT [R_25_GRUPO9]
GO
ALTER TABLE [dbo].[BEBIDA_INSUMO_GRUPO5]  WITH CHECK ADD  CONSTRAINT [R_16_GRUPO5] FOREIGN KEY([ID_INSUMO])
REFERENCES [dbo].[INSUMO_GRUPO5] ([ID_INSUMO])
GO
ALTER TABLE [dbo].[BEBIDA_INSUMO_GRUPO5] CHECK CONSTRAINT [R_16_GRUPO5]
GO
ALTER TABLE [dbo].[BEBIDA_INSUMO_GRUPO5]  WITH CHECK ADD  CONSTRAINT [R_17_GRUPO5] FOREIGN KEY([ID_BEBIDAS])
REFERENCES [dbo].[BEBIDAS_GRUPO5] ([ID_BEBIDAS])
GO
ALTER TABLE [dbo].[BEBIDA_INSUMO_GRUPO5] CHECK CONSTRAINT [R_17_GRUPO5]
GO
ALTER TABLE [dbo].[BEBIDAS_GRUPO5]  WITH CHECK ADD  CONSTRAINT [R_8_GRUPO5] FOREIGN KEY([ID_TIPO_BEBIDA])
REFERENCES [dbo].[TIPO_BEBIDA_GRUPO5] ([ID_TIPO_BEBIDA])
GO
ALTER TABLE [dbo].[BEBIDAS_GRUPO5] CHECK CONSTRAINT [R_8_GRUPO5]
GO
ALTER TABLE [dbo].[BSC_OBJETIVO_GRUPO7]  WITH CHECK ADD  CONSTRAINT [R_5_GRUPO7] FOREIGN KEY([idObjetivo])
REFERENCES [dbo].[OBJETIVO_GRUPO7] ([idObjetivo])
GO
ALTER TABLE [dbo].[BSC_OBJETIVO_GRUPO7] CHECK CONSTRAINT [R_5_GRUPO7]
GO
ALTER TABLE [dbo].[BSC_OBJETIVO_GRUPO7]  WITH CHECK ADD  CONSTRAINT [R_6_GRUPO7] FOREIGN KEY([codBsc])
REFERENCES [dbo].[BSC_GRUPO7] ([codBsc])
GO
ALTER TABLE [dbo].[BSC_OBJETIVO_GRUPO7] CHECK CONSTRAINT [R_6_GRUPO7]
GO
ALTER TABLE [dbo].[Capaciaciones_Empleado_GRUPO4]  WITH CHECK ADD  CONSTRAINT [FK_Empleado_Capaciaciones_Empleado_GRUPO4] FOREIGN KEY([cod_empleado], [cod_local], [cod_tipoC], [cod_turno])
REFERENCES [dbo].[Empleado_GRUPO4] ([cod_empleado], [cod_local], [cod_tipoC], [cod_turno])
GO
ALTER TABLE [dbo].[Capaciaciones_Empleado_GRUPO4] CHECK CONSTRAINT [FK_Empleado_Capaciaciones_Empleado_GRUPO4]
GO
ALTER TABLE [dbo].[Capaciaciones_Empleado_GRUPO4]  WITH CHECK ADD  CONSTRAINT [R_24] FOREIGN KEY([cod_Capacitacion])
REFERENCES [dbo].[Capaciaciones_GRUPO4] ([cod_Capacitacion])
GO
ALTER TABLE [dbo].[Capaciaciones_Empleado_GRUPO4] CHECK CONSTRAINT [R_24]
GO
ALTER TABLE [dbo].[Cargo_GRUPO4]  WITH CHECK ADD  CONSTRAINT [FK_Cargo_GRUPO4_Empleado_GRUPO4] FOREIGN KEY([cod_empleado], [cod_local], [cod_tipoC], [cod_turno])
REFERENCES [dbo].[Empleado_GRUPO4] ([cod_empleado], [cod_local], [cod_tipoC], [cod_turno])
GO
ALTER TABLE [dbo].[Cargo_GRUPO4] CHECK CONSTRAINT [FK_Cargo_GRUPO4_Empleado_GRUPO4]
GO
ALTER TABLE [dbo].[CATALOGO_BALANCE_GENERAL_GRUPO9]  WITH CHECK ADD  CONSTRAINT [R_15_GRUPO9] FOREIGN KEY([Id_SubCuenta])
REFERENCES [dbo].[SUBCUENTAS_GRUPO9] ([IdSubCuenta])
GO
ALTER TABLE [dbo].[CATALOGO_BALANCE_GENERAL_GRUPO9] CHECK CONSTRAINT [R_15_GRUPO9]
GO
ALTER TABLE [dbo].[CATALOGO_DE_LIBROS_DIARIOS_GRUPO9]  WITH CHECK ADD  CONSTRAINT [R_4_GRUPO9] FOREIGN KEY([IdSubCuenta])
REFERENCES [dbo].[SUBCUENTAS_GRUPO9] ([IdSubCuenta])
GO
ALTER TABLE [dbo].[CATALOGO_DE_LIBROS_DIARIOS_GRUPO9] CHECK CONSTRAINT [R_4_GRUPO9]
GO
ALTER TABLE [dbo].[CATALOGO_ESTADO_PERDIDAS_Y_GANANCIAS_GRUPO9]  WITH CHECK ADD  CONSTRAINT [R_16_GRUPO9] FOREIGN KEY([Id_SubCuenta])
REFERENCES [dbo].[SUBCUENTAS_GRUPO9] ([IdSubCuenta])
GO
ALTER TABLE [dbo].[CATALOGO_ESTADO_PERDIDAS_Y_GANANCIAS_GRUPO9] CHECK CONSTRAINT [R_16_GRUPO9]
GO
ALTER TABLE [dbo].[CATALOGO_LIBRO_BANCO_GRUPO9]  WITH CHECK ADD  CONSTRAINT [R_8_GRUPO9] FOREIGN KEY([IdSubCuenta])
REFERENCES [dbo].[SUBCUENTAS_GRUPO9] ([IdSubCuenta])
GO
ALTER TABLE [dbo].[CATALOGO_LIBRO_BANCO_GRUPO9] CHECK CONSTRAINT [R_8_GRUPO9]
GO
ALTER TABLE [dbo].[CATALOGO_LIBRO_CAJA_GRUPO9]  WITH CHECK ADD  CONSTRAINT [R_7_GRUPO9] FOREIGN KEY([IdSubCuenta])
REFERENCES [dbo].[SUBCUENTAS_GRUPO9] ([IdSubCuenta])
GO
ALTER TABLE [dbo].[CATALOGO_LIBRO_CAJA_GRUPO9] CHECK CONSTRAINT [R_7_GRUPO9]
GO
ALTER TABLE [dbo].[CATALOGO_LIBRO_INVENTARIO_GRUPO9]  WITH CHECK ADD  CONSTRAINT [R_6_GRUPO9] FOREIGN KEY([IdSubCuenta])
REFERENCES [dbo].[SUBCUENTAS_GRUPO9] ([IdSubCuenta])
GO
ALTER TABLE [dbo].[CATALOGO_LIBRO_INVENTARIO_GRUPO9] CHECK CONSTRAINT [R_6_GRUPO9]
GO
ALTER TABLE [dbo].[CLIENTE_GRUPO3]  WITH CHECK ADD  CONSTRAINT [FK_CLIENTES_GRUPO8_Categoria_GRUPO3] FOREIGN KEY([CategoriaID])
REFERENCES [dbo].[CATEGORIA_DEL_CLIENTE_GRUPO3] ([CategoriaID])
GO
ALTER TABLE [dbo].[CLIENTE_GRUPO3] CHECK CONSTRAINT [FK_CLIENTES_GRUPO8_Categoria_GRUPO3]
GO
ALTER TABLE [dbo].[COMPRAS_GRUPO8]  WITH CHECK ADD  CONSTRAINT [FK_COMPRAS_GRUPO8_cuentasporpagar_GRUPO7F] FOREIGN KEY([idcuentaporpagar])
REFERENCES [dbo].[cuentasporpagar_GRUPO7F] ([idcuentaporpagar])
GO
ALTER TABLE [dbo].[COMPRAS_GRUPO8] CHECK CONSTRAINT [FK_COMPRAS_GRUPO8_cuentasporpagar_GRUPO7F]
GO
ALTER TABLE [dbo].[COMPRAS_GRUPO8]  WITH CHECK ADD  CONSTRAINT [FK_COMPRAS_GRUPO8_VENTAS_COMPRAS_LIBROS_GRUPO9] FOREIGN KEY([id_compras_libro])
REFERENCES [dbo].[LIBROS_DE_VENTAS_COMPRAS_GRUPO9] ([IdVentasComprasLibros])
GO
ALTER TABLE [dbo].[COMPRAS_GRUPO8] CHECK CONSTRAINT [FK_COMPRAS_GRUPO8_VENTAS_COMPRAS_LIBROS_GRUPO9]
GO
ALTER TABLE [dbo].[COMPRAS_GRUPO8]  WITH CHECK ADD  CONSTRAINT [R_14_GRUPO8] FOREIGN KEY([Id_PROVEEDORES])
REFERENCES [dbo].[PROVEEDORES_GRUPO8] ([Id_PROVEEDORES])
GO
ALTER TABLE [dbo].[COMPRAS_GRUPO8] CHECK CONSTRAINT [R_14_GRUPO8]
GO
ALTER TABLE [dbo].[controlchequepropio_GRUPO7F]  WITH CHECK ADD  CONSTRAINT [R_8_GRUPO7F] FOREIGN KEY([idmediopago])
REFERENCES [dbo].[mediopago_GRUPO7F] ([idmediopago])
GO
ALTER TABLE [dbo].[controlchequepropio_GRUPO7F] CHECK CONSTRAINT [R_8_GRUPO7F]
GO
ALTER TABLE [dbo].[controlchequetercero_GRUPO7F]  WITH CHECK ADD  CONSTRAINT [R_9_GRUPO7F] FOREIGN KEY([idmediopago])
REFERENCES [dbo].[mediopago_GRUPO7F] ([idmediopago])
GO
ALTER TABLE [dbo].[controlchequetercero_GRUPO7F] CHECK CONSTRAINT [R_9_GRUPO7F]
GO
ALTER TABLE [dbo].[CUENTASBANCARIAS_GRUPO7F]  WITH CHECK ADD  CONSTRAINT [FK_CUENTASBANCARIAS_GRUPO7F_VENTA_GRUPO51] FOREIGN KEY([id_VENTAS], [Id_CLIENTES])
REFERENCES [dbo].[VENTA_GRUPO5] ([id_VENTAS], [Id_CLIENTES])
GO
ALTER TABLE [dbo].[CUENTASBANCARIAS_GRUPO7F] CHECK CONSTRAINT [FK_CUENTASBANCARIAS_GRUPO7F_VENTA_GRUPO51]
GO
ALTER TABLE [dbo].[CUENTASBANCARIAS_GRUPO7F]  WITH CHECK ADD  CONSTRAINT [R_3_GRUPO7F] FOREIGN KEY([idcuentaporpagar])
REFERENCES [dbo].[cuentasporpagar_GRUPO7F] ([idcuentaporpagar])
GO
ALTER TABLE [dbo].[CUENTASBANCARIAS_GRUPO7F] CHECK CONSTRAINT [R_3_GRUPO7F]
GO
ALTER TABLE [dbo].[CUENTASBANCARIAS_GRUPO7F]  WITH CHECK ADD  CONSTRAINT [R_4_GRUPO7F] FOREIGN KEY([idcuentasporcobrar])
REFERENCES [dbo].[cuentasporcobrar_GRUPO7F] ([idcuentasporcobrar])
GO
ALTER TABLE [dbo].[CUENTASBANCARIAS_GRUPO7F] CHECK CONSTRAINT [R_4_GRUPO7F]
GO
ALTER TABLE [dbo].[cuentasporcobrar_GRUPO7F]  WITH CHECK ADD  CONSTRAINT [R_6_GRUPO7F] FOREIGN KEY([idmediopago])
REFERENCES [dbo].[mediopago_GRUPO7F] ([idmediopago])
GO
ALTER TABLE [dbo].[cuentasporcobrar_GRUPO7F] CHECK CONSTRAINT [R_6_GRUPO7F]
GO
ALTER TABLE [dbo].[Departamento_GRUPO4]  WITH CHECK ADD  CONSTRAINT [FK_Departamento_GRUPO4_Empleado_GRUPO41] FOREIGN KEY([cod_empleado], [cod_local], [cod_tipoC], [cod_turno])
REFERENCES [dbo].[Empleado_GRUPO4] ([cod_empleado], [cod_local], [cod_tipoC], [cod_turno])
GO
ALTER TABLE [dbo].[Departamento_GRUPO4] CHECK CONSTRAINT [FK_Departamento_GRUPO4_Empleado_GRUPO41]
GO
ALTER TABLE [dbo].[Descanso_Medico_GRUPO4]  WITH CHECK ADD  CONSTRAINT [R_19_GRUPO4] FOREIGN KEY([cod_planilla], [cod_empleado], [cod_tipoC], [cod_local], [cod_turno])
REFERENCES [dbo].[Planilla_GRUPO4] ([cod_planilla], [cod_empleado], [cod_tipoC], [cod_local], [cod_turno])
GO
ALTER TABLE [dbo].[Descanso_Medico_GRUPO4] CHECK CONSTRAINT [R_19_GRUPO4]
GO
ALTER TABLE [dbo].[Empleado_GRUPO4]  WITH CHECK ADD  CONSTRAINT [FK_Empleado_GRUPO4_Local_GRUPO8P] FOREIGN KEY([ID_LOCAL])
REFERENCES [dbo].[Local_GRUPO8P] ([ID_LOCAL])
GO
ALTER TABLE [dbo].[Empleado_GRUPO4] CHECK CONSTRAINT [FK_Empleado_GRUPO4_Local_GRUPO8P]
GO
ALTER TABLE [dbo].[Empleado_GRUPO4]  WITH CHECK ADD  CONSTRAINT [FK_Empleado_GRUPO4_MESERO_GRUPO10] FOREIGN KEY([cod_empleado])
REFERENCES [dbo].[MESERO_GRUPO10] ([ID_MESERO])
GO
ALTER TABLE [dbo].[Empleado_GRUPO4] CHECK CONSTRAINT [FK_Empleado_GRUPO4_MESERO_GRUPO10]
GO
ALTER TABLE [dbo].[Empleado_GRUPO4]  WITH CHECK ADD  CONSTRAINT [FK_Empleado_GRUPO4_RESPONSABLE_GRUPO7] FOREIGN KEY([codResponsable])
REFERENCES [dbo].[RESPONSABLE_GRUPO7] ([codResponsable])
GO
ALTER TABLE [dbo].[Empleado_GRUPO4] CHECK CONSTRAINT [FK_Empleado_GRUPO4_RESPONSABLE_GRUPO7]
GO
ALTER TABLE [dbo].[Empleado_GRUPO4]  WITH CHECK ADD  CONSTRAINT [R_14_GRUPO4] FOREIGN KEY([cod_turno])
REFERENCES [dbo].[Horario_GRUPO4] ([cod_turno])
GO
ALTER TABLE [dbo].[Empleado_GRUPO4] CHECK CONSTRAINT [R_14_GRUPO4]
GO
ALTER TABLE [dbo].[Empleado_GRUPO4]  WITH CHECK ADD  CONSTRAINT [R_6_GRUPO4] FOREIGN KEY([cod_tipoC])
REFERENCES [dbo].[Tipo_de_contrato_GRUPO4] ([cod_tipoC])
GO
ALTER TABLE [dbo].[Empleado_GRUPO4] CHECK CONSTRAINT [R_6_GRUPO4]
GO
ALTER TABLE [dbo].[ESTADO_DE_PERDIDAS_Y_GANANCIAS_GRUPO9]  WITH CHECK ADD  CONSTRAINT [R_21_GRUPO9] FOREIGN KEY([IdLibrosDiarios])
REFERENCES [dbo].[LIBRO_DIARIO_GRUPO9] ([IdLibrosDiarios])
GO
ALTER TABLE [dbo].[ESTADO_DE_PERDIDAS_Y_GANANCIAS_GRUPO9] CHECK CONSTRAINT [R_21_GRUPO9]
GO
ALTER TABLE [dbo].[ESTADO_DE_PERDIDAS_Y_GANANCIAS_GRUPO9]  WITH CHECK ADD  CONSTRAINT [R_22_GRUPO9] FOREIGN KEY([IdLibroInventario])
REFERENCES [dbo].[LIBRO_INVENTARIO_GRUPO9] ([IdLibroInventario])
GO
ALTER TABLE [dbo].[ESTADO_DE_PERDIDAS_Y_GANANCIAS_GRUPO9] CHECK CONSTRAINT [R_22_GRUPO9]
GO
ALTER TABLE [dbo].[ESTADO_DE_PERDIDAS_Y_GANANCIAS_GRUPO9]  WITH CHECK ADD  CONSTRAINT [R_23_GRUPO9] FOREIGN KEY([IdLibroCaja])
REFERENCES [dbo].[LIBRO_CAJA_GRUPO9] ([IdLibroCaja])
GO
ALTER TABLE [dbo].[ESTADO_DE_PERDIDAS_Y_GANANCIAS_GRUPO9] CHECK CONSTRAINT [R_23_GRUPO9]
GO
ALTER TABLE [dbo].[ESTADO_DE_PERDIDAS_Y_GANANCIAS_GRUPO9]  WITH CHECK ADD  CONSTRAINT [R_24_GRUPO9] FOREIGN KEY([IdLibroBanco])
REFERENCES [dbo].[LIBRO_BANCO_GRUPO9] ([IdLibroBanco])
GO
ALTER TABLE [dbo].[ESTADO_DE_PERDIDAS_Y_GANANCIAS_GRUPO9] CHECK CONSTRAINT [R_24_GRUPO9]
GO
ALTER TABLE [dbo].[ESTADO_DE_PERDIDAS_Y_GANANCIAS_GRUPO9]  WITH CHECK ADD  CONSTRAINT [R_26_GRUPO9] FOREIGN KEY([IdCatPerdGanancias])
REFERENCES [dbo].[CATALOGO_ESTADO_PERDIDAS_Y_GANANCIAS_GRUPO9] ([IdCatPerdGanancias])
GO
ALTER TABLE [dbo].[ESTADO_DE_PERDIDAS_Y_GANANCIAS_GRUPO9] CHECK CONSTRAINT [R_26_GRUPO9]
GO
ALTER TABLE [dbo].[Evaluación_de_desempeño_GRUPO4]  WITH CHECK ADD  CONSTRAINT [FK_Evaluación_de_desempeño_GRUPO4_Empleado_GRUPO41] FOREIGN KEY([cod_empleado], [cod_local], [cod_tipoC], [cod_turno])
REFERENCES [dbo].[Empleado_GRUPO4] ([cod_empleado], [cod_local], [cod_tipoC], [cod_turno])
GO
ALTER TABLE [dbo].[Evaluación_de_desempeño_GRUPO4] CHECK CONSTRAINT [FK_Evaluación_de_desempeño_GRUPO4_Empleado_GRUPO41]
GO
ALTER TABLE [dbo].[Gratificación_GRUPO4]  WITH CHECK ADD  CONSTRAINT [R_20_GRUPO4] FOREIGN KEY([cod_planilla], [cod_empleado], [cod_tipoC], [cod_local], [cod_turno])
REFERENCES [dbo].[Planilla_GRUPO4] ([cod_planilla], [cod_empleado], [cod_tipoC], [cod_local], [cod_turno])
GO
ALTER TABLE [dbo].[Gratificación_GRUPO4] CHECK CONSTRAINT [R_20_GRUPO4]
GO
ALTER TABLE [dbo].[Horario_Horas_extras_GRUPO4]  WITH CHECK ADD  CONSTRAINT [R_31] FOREIGN KEY([cod_turno])
REFERENCES [dbo].[Horario_GRUPO4] ([cod_turno])
GO
ALTER TABLE [dbo].[Horario_Horas_extras_GRUPO4] CHECK CONSTRAINT [R_31]
GO
ALTER TABLE [dbo].[Horario_Horas_extras_GRUPO4]  WITH CHECK ADD  CONSTRAINT [R_32] FOREIGN KEY([cod_horas_extras], [cod_asistencia], [cod_turno])
REFERENCES [dbo].[Horas_extras_GRUPO4] ([cod_horas_extras], [cod_asistencia], [cod_turno])
GO
ALTER TABLE [dbo].[Horario_Horas_extras_GRUPO4] CHECK CONSTRAINT [R_32]
GO
ALTER TABLE [dbo].[Horas_extras_GRUPO4]  WITH CHECK ADD  CONSTRAINT [R_21_GRUPO4] FOREIGN KEY([cod_asistencia], [cod_turno])
REFERENCES [dbo].[Asistencia_GRUPO4] ([cod_asistencia], [cod_turno])
GO
ALTER TABLE [dbo].[Horas_extras_GRUPO4] CHECK CONSTRAINT [R_21_GRUPO4]
GO
ALTER TABLE [dbo].[INDICADOR_GRUPO7]  WITH CHECK ADD  CONSTRAINT [R_4_GRUPO7] FOREIGN KEY([idObjetivo])
REFERENCES [dbo].[OBJETIVO_GRUPO7] ([idObjetivo])
GO
ALTER TABLE [dbo].[INDICADOR_GRUPO7] CHECK CONSTRAINT [R_4_GRUPO7]
GO
ALTER TABLE [dbo].[INDICADOR_GRUPO7]  WITH CHECK ADD  CONSTRAINT [R_7_GRUPO7] FOREIGN KEY([codResponsable])
REFERENCES [dbo].[RESPONSABLE_GRUPO7] ([codResponsable])
GO
ALTER TABLE [dbo].[INDICADOR_GRUPO7] CHECK CONSTRAINT [R_7_GRUPO7]
GO
ALTER TABLE [dbo].[INSUMO_PLATO_GRUPO5]  WITH CHECK ADD  CONSTRAINT [R_18_GRUPO5] FOREIGN KEY([ID_INSUMO])
REFERENCES [dbo].[INSUMO_GRUPO5] ([ID_INSUMO])
GO
ALTER TABLE [dbo].[INSUMO_PLATO_GRUPO5] CHECK CONSTRAINT [R_18_GRUPO5]
GO
ALTER TABLE [dbo].[INSUMO_PLATO_GRUPO5]  WITH CHECK ADD  CONSTRAINT [R_19_GRUPO5] FOREIGN KEY([ID_PLATO])
REFERENCES [dbo].[PLATO_GRUPO5] ([ID_PLATO])
GO
ALTER TABLE [dbo].[INSUMO_PLATO_GRUPO5] CHECK CONSTRAINT [R_19_GRUPO5]
GO
ALTER TABLE [dbo].[LIBRO_BANCO_GRUPO9]  WITH CHECK ADD  CONSTRAINT [R_11_GRUPO9] FOREIGN KEY([IdCatLibBanco])
REFERENCES [dbo].[CATALOGO_LIBRO_BANCO_GRUPO9] ([IdCatLibBanco])
GO
ALTER TABLE [dbo].[LIBRO_BANCO_GRUPO9] CHECK CONSTRAINT [R_11_GRUPO9]
GO
ALTER TABLE [dbo].[LIBRO_CAJA_GRUPO9]  WITH CHECK ADD  CONSTRAINT [R_10_GRUPO9] FOREIGN KEY([IdCatLibCaja])
REFERENCES [dbo].[CATALOGO_LIBRO_CAJA_GRUPO9] ([IdCatLibCaja])
GO
ALTER TABLE [dbo].[LIBRO_CAJA_GRUPO9] CHECK CONSTRAINT [R_10_GRUPO9]
GO
ALTER TABLE [dbo].[LIBRO_DIARIO_GRUPO9]  WITH CHECK ADD  CONSTRAINT [FK_LIBRO_DIARIO_GRUPO9_VENTAS_COMPRAS_LIBROS_GRUPO9] FOREIGN KEY([IdVentasComprasLibros])
REFERENCES [dbo].[LIBROS_DE_VENTAS_COMPRAS_GRUPO9] ([IdVentasComprasLibros])
GO
ALTER TABLE [dbo].[LIBRO_DIARIO_GRUPO9] CHECK CONSTRAINT [FK_LIBRO_DIARIO_GRUPO9_VENTAS_COMPRAS_LIBROS_GRUPO9]
GO
ALTER TABLE [dbo].[LIBRO_DIARIO_GRUPO9]  WITH CHECK ADD  CONSTRAINT [R_12_GRUPO9] FOREIGN KEY([IDCatLibDiario])
REFERENCES [dbo].[CATALOGO_DE_LIBROS_DIARIOS_GRUPO9] ([IDCatLibDiario])
GO
ALTER TABLE [dbo].[LIBRO_DIARIO_GRUPO9] CHECK CONSTRAINT [R_12_GRUPO9]
GO
ALTER TABLE [dbo].[LIBRO_INVENTARIO_GRUPO9]  WITH CHECK ADD  CONSTRAINT [FK_LIBRO_INVENTARIO_GRUPO9_COMPRAS_GRUPO8] FOREIGN KEY([Id_COMPRAS], [Id_PROVEEDORES])
REFERENCES [dbo].[COMPRAS_GRUPO8] ([Id_COMPRAS], [Id_PROVEEDORES])
GO
ALTER TABLE [dbo].[LIBRO_INVENTARIO_GRUPO9] CHECK CONSTRAINT [FK_LIBRO_INVENTARIO_GRUPO9_COMPRAS_GRUPO8]
GO
ALTER TABLE [dbo].[LIBRO_INVENTARIO_GRUPO9]  WITH CHECK ADD  CONSTRAINT [R_9_GRUPO9] FOREIGN KEY([IdCatLibInventario])
REFERENCES [dbo].[CATALOGO_LIBRO_INVENTARIO_GRUPO9] ([IdCatLibInventario])
GO
ALTER TABLE [dbo].[LIBRO_INVENTARIO_GRUPO9] CHECK CONSTRAINT [R_9_GRUPO9]
GO
ALTER TABLE [dbo].[LIBRO_MAYOR_GRUPO9]  WITH CHECK ADD  CONSTRAINT [R_14_GRUPO9] FOREIGN KEY([IdLibrosDiarios])
REFERENCES [dbo].[LIBRO_DIARIO_GRUPO9] ([IdLibrosDiarios])
GO
ALTER TABLE [dbo].[LIBRO_MAYOR_GRUPO9] CHECK CONSTRAINT [R_14_GRUPO9]
GO
ALTER TABLE [dbo].[LIBROS_DE_VENTAS_COMPRAS_GRUPO9]  WITH CHECK ADD  CONSTRAINT [FK_VENTAS_COMPRAS_LIBROS_GRUPO9_VENTA_GRUPO51] FOREIGN KEY([id_VENTAS], [Id_CLIENTES])
REFERENCES [dbo].[VENTA_GRUPO5] ([id_VENTAS], [Id_CLIENTES])
GO
ALTER TABLE [dbo].[LIBROS_DE_VENTAS_COMPRAS_GRUPO9] CHECK CONSTRAINT [FK_VENTAS_COMPRAS_LIBROS_GRUPO9_VENTA_GRUPO51]
GO
ALTER TABLE [dbo].[LINEADETALLESCOMPRA_GRUPO8]  WITH CHECK ADD  CONSTRAINT [FK_LINEADETALLESCOMPRA_GRUPO8_COMPRAS_GRUPO8] FOREIGN KEY([Id_COMPRAS], [Id_PROVEEDORES])
REFERENCES [dbo].[COMPRAS_GRUPO8] ([Id_COMPRAS], [Id_PROVEEDORES])
GO
ALTER TABLE [dbo].[LINEADETALLESCOMPRA_GRUPO8] CHECK CONSTRAINT [FK_LINEADETALLESCOMPRA_GRUPO8_COMPRAS_GRUPO8]
GO
ALTER TABLE [dbo].[LINEADETALLESCOMPRA_GRUPO8]  WITH CHECK ADD  CONSTRAINT [R_18_GRUPO8] FOREIGN KEY([Id_PRODUCTOS])
REFERENCES [dbo].[PRODUCTOS_GRUPO8] ([Id_PRODUCTOS])
GO
ALTER TABLE [dbo].[LINEADETALLESCOMPRA_GRUPO8] CHECK CONSTRAINT [R_18_GRUPO8]
GO
ALTER TABLE [dbo].[LINEADETALLEVENTA_GRUPO8]  WITH CHECK ADD  CONSTRAINT [FK_LINEADETALLEVENTA_GRUPO8_PRODUCTOS_GRUPO8] FOREIGN KEY([Id_PRODUCTOS])
REFERENCES [dbo].[PRODUCTOS_GRUPO8] ([Id_PRODUCTOS])
GO
ALTER TABLE [dbo].[LINEADETALLEVENTA_GRUPO8] CHECK CONSTRAINT [FK_LINEADETALLEVENTA_GRUPO8_PRODUCTOS_GRUPO8]
GO
ALTER TABLE [dbo].[LINEADETALLEVENTA_GRUPO8]  WITH CHECK ADD  CONSTRAINT [FK_LINEADETALLEVENTA_GRUPO8_PROMOCION_VENTAS5P1] FOREIGN KEY([id_promo])
REFERENCES [dbo].[PROMOCION_VENTAS5P] ([id_promo])
GO
ALTER TABLE [dbo].[LINEADETALLEVENTA_GRUPO8] CHECK CONSTRAINT [FK_LINEADETALLEVENTA_GRUPO8_PROMOCION_VENTAS5P1]
GO
ALTER TABLE [dbo].[LINEADETALLEVENTA_GRUPO8]  WITH CHECK ADD  CONSTRAINT [FK_LINEADETALLEVENTA_GRUPO8_VENTA_GRUPO5] FOREIGN KEY([Id_VENTAS], [Id_CLIENTES])
REFERENCES [dbo].[VENTA_GRUPO5] ([id_VENTAS], [Id_CLIENTES])
GO
ALTER TABLE [dbo].[LINEADETALLEVENTA_GRUPO8] CHECK CONSTRAINT [FK_LINEADETALLEVENTA_GRUPO8_VENTA_GRUPO5]
GO
ALTER TABLE [dbo].[Local_GRUPO8P]  WITH CHECK ADD  CONSTRAINT [FK_Local_GRUPO8_PEDIDO_GRUPO5] FOREIGN KEY([ID_LOCAL])
REFERENCES [dbo].[PEDIDO_GRUPO5] ([ID_PEDIDO])
GO
ALTER TABLE [dbo].[Local_GRUPO8P] CHECK CONSTRAINT [FK_Local_GRUPO8_PEDIDO_GRUPO5]
GO
ALTER TABLE [dbo].[Local_GRUPO8P]  WITH CHECK ADD  CONSTRAINT [FK_Local_GRUPO8P_CLIENTE_GRUPO3] FOREIGN KEY([Id_CLIENTES])
REFERENCES [dbo].[CLIENTE_GRUPO3] ([Id_CLIENTES])
GO
ALTER TABLE [dbo].[Local_GRUPO8P] CHECK CONSTRAINT [FK_Local_GRUPO8P_CLIENTE_GRUPO3]
GO
ALTER TABLE [dbo].[MESA_GRUPO5]  WITH CHECK ADD  CONSTRAINT [FK_MESA_GRUPO5_Local_GRUPO8P] FOREIGN KEY([ID_LOCAL])
REFERENCES [dbo].[Local_GRUPO8P] ([ID_LOCAL])
GO
ALTER TABLE [dbo].[MESA_GRUPO5] CHECK CONSTRAINT [FK_MESA_GRUPO5_Local_GRUPO8P]
GO
ALTER TABLE [dbo].[MESA_GRUPO5]  WITH CHECK ADD  CONSTRAINT [FK_MESA_GRUPO5_Revervas_GRUPO3] FOREIGN KEY([ReservaID])
REFERENCES [dbo].[Revervas_GRUPO3] ([ReservaID])
GO
ALTER TABLE [dbo].[MESA_GRUPO5] CHECK CONSTRAINT [FK_MESA_GRUPO5_Revervas_GRUPO3]
GO
ALTER TABLE [dbo].[MESERO_GRUPO10]  WITH CHECK ADD  CONSTRAINT [FK_MESERO_GRUPO10_CLIENTE_GRUPO3] FOREIGN KEY([ID_CLIENTES])
REFERENCES [dbo].[CLIENTE_GRUPO3] ([Id_CLIENTES])
GO
ALTER TABLE [dbo].[MESERO_GRUPO10] CHECK CONSTRAINT [FK_MESERO_GRUPO10_CLIENTE_GRUPO3]
GO
ALTER TABLE [dbo].[MESERO_GRUPO10]  WITH CHECK ADD  CONSTRAINT [FK_MESERO_GRUPO10_PEDIDO_GRUPO5] FOREIGN KEY([ID_PEDIDO])
REFERENCES [dbo].[PEDIDO_GRUPO5] ([ID_PEDIDO])
GO
ALTER TABLE [dbo].[MESERO_GRUPO10] CHECK CONSTRAINT [FK_MESERO_GRUPO10_PEDIDO_GRUPO5]
GO
ALTER TABLE [dbo].[MOVIMIENTOS_GRUPO8]  WITH CHECK ADD  CONSTRAINT [FK_MOVIMIENTOS_GRUPO8_VENTA_GRUPO5] FOREIGN KEY([Id_VENTAS], [Id_CLIENTES])
REFERENCES [dbo].[VENTA_GRUPO5] ([id_VENTAS], [Id_CLIENTES])
GO
ALTER TABLE [dbo].[MOVIMIENTOS_GRUPO8] CHECK CONSTRAINT [FK_MOVIMIENTOS_GRUPO8_VENTA_GRUPO5]
GO
ALTER TABLE [dbo].[MOVIMIENTOS_GRUPO8]  WITH CHECK ADD  CONSTRAINT [R_21_GRUPO8] FOREIGN KEY([Id_COMPRAS], [Id_PROVEEDORES])
REFERENCES [dbo].[COMPRAS_GRUPO8] ([Id_COMPRAS], [Id_PROVEEDORES])
GO
ALTER TABLE [dbo].[MOVIMIENTOS_GRUPO8] CHECK CONSTRAINT [R_21_GRUPO8]
GO
ALTER TABLE [dbo].[MOVIMIENTOS_GRUPO8]  WITH CHECK ADD  CONSTRAINT [R_9_GRUPO8] FOREIGN KEY([Id_PRODUCTOS])
REFERENCES [dbo].[PRODUCTOS_GRUPO8] ([Id_PRODUCTOS])
GO
ALTER TABLE [dbo].[MOVIMIENTOS_GRUPO8] CHECK CONSTRAINT [R_9_GRUPO8]
GO
ALTER TABLE [dbo].[OBJETIVO_GRUPO7]  WITH CHECK ADD  CONSTRAINT [R_3_GRUPO7] FOREIGN KEY([idPerspectiva])
REFERENCES [dbo].[PERSPECTIVA_GRUPO7] ([idPerspectiva])
GO
ALTER TABLE [dbo].[OBJETIVO_GRUPO7] CHECK CONSTRAINT [R_3_GRUPO7]
GO
ALTER TABLE [dbo].[ORDENPEDIDOS_GRUPO8]  WITH CHECK ADD  CONSTRAINT [R_5_GRUPO8] FOREIGN KEY([Id_PRODUCTOS])
REFERENCES [dbo].[PRODUCTOS_GRUPO8] ([Id_PRODUCTOS])
GO
ALTER TABLE [dbo].[ORDENPEDIDOS_GRUPO8] CHECK CONSTRAINT [R_5_GRUPO8]
GO
ALTER TABLE [dbo].[PEDIDO_BEBIDA_GRUPO5]  WITH CHECK ADD  CONSTRAINT [R_6_GRUPO5] FOREIGN KEY([ID_PEDIDO])
REFERENCES [dbo].[PEDIDO_GRUPO5] ([ID_PEDIDO])
GO
ALTER TABLE [dbo].[PEDIDO_BEBIDA_GRUPO5] CHECK CONSTRAINT [R_6_GRUPO5]
GO
ALTER TABLE [dbo].[PEDIDO_BEBIDA_GRUPO5]  WITH CHECK ADD  CONSTRAINT [R_9_GRUPO5] FOREIGN KEY([ID_BEBIDAS])
REFERENCES [dbo].[BEBIDAS_GRUPO5] ([ID_BEBIDAS])
GO
ALTER TABLE [dbo].[PEDIDO_BEBIDA_GRUPO5] CHECK CONSTRAINT [R_9_GRUPO5]
GO
ALTER TABLE [dbo].[PEDIDO_PLATO_GRUPO5]  WITH CHECK ADD  CONSTRAINT [R_15_GRUPO5] FOREIGN KEY([ID_PLATO])
REFERENCES [dbo].[PLATO_GRUPO5] ([ID_PLATO])
GO
ALTER TABLE [dbo].[PEDIDO_PLATO_GRUPO5] CHECK CONSTRAINT [R_15_GRUPO5]
GO
ALTER TABLE [dbo].[PEDIDO_PLATO_GRUPO5]  WITH CHECK ADD  CONSTRAINT [R_7_GRUPO5] FOREIGN KEY([ID_PEDIDO])
REFERENCES [dbo].[PEDIDO_GRUPO5] ([ID_PEDIDO])
GO
ALTER TABLE [dbo].[PEDIDO_PLATO_GRUPO5] CHECK CONSTRAINT [R_7_GRUPO5]
GO
ALTER TABLE [dbo].[Planilla_Asistencia_GRUPO4]  WITH CHECK ADD  CONSTRAINT [R_27] FOREIGN KEY([cod_planilla], [cod_empleado], [cod_tipoC], [cod_local], [cod_turno])
REFERENCES [dbo].[Planilla_GRUPO4] ([cod_planilla], [cod_empleado], [cod_tipoC], [cod_local], [cod_turno])
GO
ALTER TABLE [dbo].[Planilla_Asistencia_GRUPO4] CHECK CONSTRAINT [R_27]
GO
ALTER TABLE [dbo].[Planilla_Asistencia_GRUPO4]  WITH CHECK ADD  CONSTRAINT [R_28] FOREIGN KEY([cod_asistencia], [cod_turno])
REFERENCES [dbo].[Asistencia_GRUPO4] ([cod_asistencia], [cod_turno])
GO
ALTER TABLE [dbo].[Planilla_Asistencia_GRUPO4] CHECK CONSTRAINT [R_28]
GO
ALTER TABLE [dbo].[Planilla_Descuento_GRUPO4]  WITH CHECK ADD  CONSTRAINT [R_29] FOREIGN KEY([cod_planilla], [cod_empleado], [cod_tipoC], [cod_local], [cod_turno])
REFERENCES [dbo].[Planilla_GRUPO4] ([cod_planilla], [cod_empleado], [cod_tipoC], [cod_local], [cod_turno])
GO
ALTER TABLE [dbo].[Planilla_Descuento_GRUPO4] CHECK CONSTRAINT [R_29]
GO
ALTER TABLE [dbo].[Planilla_Descuento_GRUPO4]  WITH CHECK ADD  CONSTRAINT [R_30] FOREIGN KEY([cod_desc])
REFERENCES [dbo].[Descuento_GRUPO4] ([cod_desc])
GO
ALTER TABLE [dbo].[Planilla_Descuento_GRUPO4] CHECK CONSTRAINT [R_30]
GO
ALTER TABLE [dbo].[PLATO_GRUPO5]  WITH CHECK ADD  CONSTRAINT [R_14_GRUPO5] FOREIGN KEY([ID_TIPO_PLATO])
REFERENCES [dbo].[TIPO_PLATO_GRUPO5] ([ID_TIPO_PLATO])
GO
ALTER TABLE [dbo].[PLATO_GRUPO5] CHECK CONSTRAINT [R_14_GRUPO5]
GO
ALTER TABLE [dbo].[PROMOCION_VENTAS5P]  WITH CHECK ADD  CONSTRAINT [FK_PROMOCION_VENTAS5P_BSC_GRUPO71] FOREIGN KEY([codBsc])
REFERENCES [dbo].[BSC_GRUPO7] ([codBsc])
GO
ALTER TABLE [dbo].[PROMOCION_VENTAS5P] CHECK CONSTRAINT [FK_PROMOCION_VENTAS5P_BSC_GRUPO71]
GO
ALTER TABLE [dbo].[PROMOCION_VENTAS5P]  WITH CHECK ADD  CONSTRAINT [FK_PROMOCION_VENTAS5P_PRODUCTOS_GRUPO81] FOREIGN KEY([Id_PRODUCTOS])
REFERENCES [dbo].[PRODUCTOS_GRUPO8] ([Id_PRODUCTOS])
GO
ALTER TABLE [dbo].[PROMOCION_VENTAS5P] CHECK CONSTRAINT [FK_PROMOCION_VENTAS5P_PRODUCTOS_GRUPO81]
GO
ALTER TABLE [dbo].[PROVEEDORES_GRUPO8]  WITH CHECK ADD  CONSTRAINT [FK_PROVEEDORES_GRUPO8_INSUMO_GRUPO5] FOREIGN KEY([ID_INSUMO])
REFERENCES [dbo].[INSUMO_GRUPO5] ([ID_INSUMO])
GO
ALTER TABLE [dbo].[PROVEEDORES_GRUPO8] CHECK CONSTRAINT [FK_PROVEEDORES_GRUPO8_INSUMO_GRUPO5]
GO
ALTER TABLE [dbo].[PROVEEDORESXPRODUCTO_GRUPO8]  WITH CHECK ADD  CONSTRAINT [R_17_GRUPO8] FOREIGN KEY([Id_PROVEEDORES])
REFERENCES [dbo].[PROVEEDORES_GRUPO8] ([Id_PROVEEDORES])
GO
ALTER TABLE [dbo].[PROVEEDORESXPRODUCTO_GRUPO8] CHECK CONSTRAINT [R_17_GRUPO8]
GO
ALTER TABLE [dbo].[PROVEEDORESXPRODUCTO_GRUPO8]  WITH CHECK ADD  CONSTRAINT [R_8_GRUPO8] FOREIGN KEY([Id_PRODUCTOS])
REFERENCES [dbo].[PRODUCTOS_GRUPO8] ([Id_PRODUCTOS])
GO
ALTER TABLE [dbo].[PROVEEDORESXPRODUCTO_GRUPO8] CHECK CONSTRAINT [R_8_GRUPO8]
GO
ALTER TABLE [dbo].[RECETAXPRODUCTOS_GRUPO8]  WITH CHECK ADD  CONSTRAINT [R_3_GRUPO8] FOREIGN KEY([Id_PRODUCTOS])
REFERENCES [dbo].[PRODUCTOS_GRUPO8] ([Id_PRODUCTOS])
GO
ALTER TABLE [dbo].[RECETAXPRODUCTOS_GRUPO8] CHECK CONSTRAINT [R_3_GRUPO8]
GO
ALTER TABLE [dbo].[RECETAXPRODUCTOS_GRUPO8]  WITH CHECK ADD  CONSTRAINT [R_4_GRUPO8] FOREIGN KEY([Id_RECETAS])
REFERENCES [dbo].[RECETAS_GRUPO8] ([Id_RECETAS])
GO
ALTER TABLE [dbo].[RECETAXPRODUCTOS_GRUPO8] CHECK CONSTRAINT [R_4_GRUPO8]
GO
ALTER TABLE [dbo].[Revervas_GRUPO3]  WITH CHECK ADD  CONSTRAINT [FK_Revervas_GRUPO3_CLIENTE_GRUPO3] FOREIGN KEY([Id_CLIENTES])
REFERENCES [dbo].[CLIENTE_GRUPO3] ([Id_CLIENTES])
GO
ALTER TABLE [dbo].[Revervas_GRUPO3] CHECK CONSTRAINT [FK_Revervas_GRUPO3_CLIENTE_GRUPO3]
GO
ALTER TABLE [dbo].[SOLICITUDCOMPRAS_GRUPO8]  WITH CHECK ADD  CONSTRAINT [R_16_GRUPO8] FOREIGN KEY([Id_COMPRAS], [Id_PROVEEDORES])
REFERENCES [dbo].[COMPRAS_GRUPO8] ([Id_COMPRAS], [Id_PROVEEDORES])
GO
ALTER TABLE [dbo].[SOLICITUDCOMPRAS_GRUPO8] CHECK CONSTRAINT [R_16_GRUPO8]
GO
ALTER TABLE [dbo].[SOLICITUDCOMPRAS_GRUPO8]  WITH CHECK ADD  CONSTRAINT [R_7_GRUPO8] FOREIGN KEY([Id_PRODUCTOS])
REFERENCES [dbo].[PRODUCTOS_GRUPO8] ([Id_PRODUCTOS])
GO
ALTER TABLE [dbo].[SOLICITUDCOMPRAS_GRUPO8] CHECK CONSTRAINT [R_7_GRUPO8]
GO
ALTER TABLE [dbo].[SUBCUENTAS_GRUPO9]  WITH CHECK ADD  CONSTRAINT [FK_SUBCUENTAS_GRUPO9_CUENTASBANCARIAS_GRUPO7F1] FOREIGN KEY([idcuentasbancarias])
REFERENCES [dbo].[CUENTASBANCARIAS_GRUPO7F] ([idcuentasbancarias])
GO
ALTER TABLE [dbo].[SUBCUENTAS_GRUPO9] CHECK CONSTRAINT [FK_SUBCUENTAS_GRUPO9_CUENTASBANCARIAS_GRUPO7F1]
GO
ALTER TABLE [dbo].[USUA_MODUL_GRUPO1]  WITH CHECK ADD  CONSTRAINT [R_9_GRUPO1] FOREIGN KEY([id_modulo])
REFERENCES [dbo].[MODULOS_GRUPO1] ([id_modulo])
GO
ALTER TABLE [dbo].[USUA_MODUL_GRUPO1] CHECK CONSTRAINT [R_9_GRUPO1]
GO
ALTER TABLE [dbo].[USUARIO_CONTRA_GRUPO4]  WITH CHECK ADD  CONSTRAINT [FK_USUARIO_CONTRA_GRUPO4_Empleado_GRUPO4] FOREIGN KEY([cod_empleado], [cod_local], [cod_tipoC], [cod_turno])
REFERENCES [dbo].[Empleado_GRUPO4] ([cod_empleado], [cod_local], [cod_tipoC], [cod_turno])
GO
ALTER TABLE [dbo].[USUARIO_CONTRA_GRUPO4] CHECK CONSTRAINT [FK_USUARIO_CONTRA_GRUPO4_Empleado_GRUPO4]
GO
ALTER TABLE [dbo].[USUARIO_CONTRA_GRUPO4]  WITH CHECK ADD  CONSTRAINT [FK_USUARIO_CONTRA_GRUPO4_PERMISO_GRUPO1] FOREIGN KEY([id_permiso])
REFERENCES [dbo].[PERMISO_GRUPO1] ([id_permiso])
GO
ALTER TABLE [dbo].[USUARIO_CONTRA_GRUPO4] CHECK CONSTRAINT [FK_USUARIO_CONTRA_GRUPO4_PERMISO_GRUPO1]
GO
ALTER TABLE [dbo].[USUARIO_CONTRA_GRUPO4]  WITH CHECK ADD  CONSTRAINT [FK_USUARIO_CONTRA_GRUPO4_USUA_MODUL_GRUPO1] FOREIGN KEY([id_modusu])
REFERENCES [dbo].[USUA_MODUL_GRUPO1] ([id_modusu])
GO
ALTER TABLE [dbo].[USUARIO_CONTRA_GRUPO4] CHECK CONSTRAINT [FK_USUARIO_CONTRA_GRUPO4_USUA_MODUL_GRUPO1]
GO
ALTER TABLE [dbo].[VENTA_GRUPO5]  WITH CHECK ADD  CONSTRAINT [FK_VENTA_GRUPO5_CLIENTES_GRUPO8] FOREIGN KEY([Id_CLIENTES])
REFERENCES [dbo].[CLIENTE_GRUPO3] ([Id_CLIENTES])
GO
ALTER TABLE [dbo].[VENTA_GRUPO5] CHECK CONSTRAINT [FK_VENTA_GRUPO5_CLIENTES_GRUPO8]
GO
ALTER TABLE [dbo].[VENTA_GRUPO5]  WITH CHECK ADD  CONSTRAINT [FK_VENTA_GRUPO5_PEDIDO_GRUPO5] FOREIGN KEY([ID_PEDIDO])
REFERENCES [dbo].[PEDIDO_GRUPO5] ([ID_PEDIDO])
GO
ALTER TABLE [dbo].[VENTA_GRUPO5] CHECK CONSTRAINT [FK_VENTA_GRUPO5_PEDIDO_GRUPO5]
GO
