<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/favicon2.png">
    <title>Dashboard RR.HH</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/dashboard.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
  </head>
  <body>
    <header>
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">El Hornero</a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Configuraciones</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Perfil</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Ayuda</a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <div class="container-fluid">
      <div class="row">
        <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
          <ul class="nav nav-pills flex-column">
            <li class="nav-item">
              <a class="nav-link" href="dashboard.html">Vista</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="planilla.html">Planilla </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="asistencia.html">Asistencia</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" href="capacitaciones.php">Capacitaciones<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="reportes.html">Reportes</a>
            </li>
          </ul>
        </nav>
        <main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
          <h1>Area de Recursos Humanos</h1>
          <center><h2>Capacitaciones</h2></center>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>##</th>
                  <th>Nombres</th>
                  <th>Apellidos</th>
                  <th>Capacitacion 01</th>
                  <th>Capacitacion 02</th>
                  <th>Capacitacion 03</th>
                  <th>Capacitacion 04</th>
                </tr>
              </thead>
              <tbody>
              <?php 
                  $consulta = "SELECT * FROM capacitaciones_GRUPO4";
                  $ejecutar = sqlsrv_query($con, $consulta);
                  $i = 0;

                  while($fila = sqlsrv_fetch_array($ejecutar)){
                  $id_cap = $fila['id_capacitaciones'];
                  $id_emp = $fila['emp_id'];
                  $nombres = $fila['emp_nombres'];
                  $apellidos = $fila['emp_apellidos'];
                  $cap01 = $fila['capacitacion_01'];
                  $cap02 = $fila['capacitacion_02'];
                  $cap03 = $fila['capacitacion_03'];
                  $cap04 = $fila['capacitacion_04'];
                  $i++;
              ?>
                  <tr>
                    <td><?php echo $id_cap ?></td>
                    <td><?php echo $id_emp ?></td>
                    <td><?php echo $nombres ?></td>
                    <td><?php echo $apellidos ?></td>
                    <td><?php echo $cap01 ?></td>
                    <td><?php echo $cap02 ?></td>
                    <td><?php echo $cap03 ?></td>
                    <td><?php echo $cap04 ?></td>
                  </tr>
                  <?php } ?>

                <tr>
                  <td></td>
                  <td>1,001</td>
                  <td>Diego</td>
                  <td>Quispe Jimenez</td>
                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                  <td><i class="fa fa-check" aria-hidden="true"></i></td>                  
                </tr>
                <tr>
                  <<td></td>
                  <td>1,005</td>
                  <td>Jahaira</td>
                  <td>Casma</td>
                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                </tr>
              </tbody>
            </table>
          </div>
        </main>
      </div>
    </div>
    <script src="js/jquery-3.2.1.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-3.2.1.js"><\/script>')</script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/holder.js"></script>
    <script src="js/react.js"></script>
    <script src="js/vue.js"></script>
    <script src="js/angular.js"></script>
  </body>
</html>